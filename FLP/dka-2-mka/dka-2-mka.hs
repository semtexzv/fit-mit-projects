{-
    Author: Michal Hornický <xhorni14@stud.fit.vutbr.cz>
-}
import Data.Foldable hiding (any,foldl,concat,all)
import System.IO
import System.Environment
import Data.List (findIndex, intercalate)
import qualified Data.Set as Set
import System.Console.GetOpt
import Debug.Trace

-- Data type describing singe automaton state
data State = State {
    stateInner :: String
} deriving(Show,Ord,Eq)

-- Data type describing input symbol
data Sym = Sym {
    symInner :: Char
} deriving(Show,Ord,Eq)

-- Data type describing single automaton transition
data Trans = Trans  {
    transStart :: State,
    transSym :: Sym,
    transEnd :: State
} deriving(Show,Ord,Eq)

-- Data type describing Finite state automaton
data Fsm = Fsm {
    states :: Set.Set State,
    symbols :: Set.Set Sym,
    transitions :: Set.Set Trans,
    start :: State,
    ends :: Set.Set State
} deriving(Show,Eq,Ord)

-- Split String with provided delimiter
splitWithDelim  :: Char -> [Char] -> [[Char]]
splitWithDelim delim str = split [] str
    where
    split first [] = [first]
    split first (x:rest) = if x == delim
        then [first] ++ (split [] rest)
        else split (first ++ [x]) rest

-- Split string on commas
splitCommaSep :: String -> [String]
splitCommaSep str = splitWithDelim ',' str

-- Parse list of states
parseStateList :: String -> Set.Set State
parseStateList line = Set.fromList (fmap State (splitCommaSep line) )

-- Parse singe transition
parseTransition :: String -> Trans
parseTransition line = Trans (State start) (Sym (head sym) ) (State (head end ))
        where (start:sym:end) = splitCommaSep line

-- Parse all transitions from list of lines
parseAllTransitions :: [String] -> Set.Set Trans
parseAllTransitions lines = Set.fromList $ fmap parseTransition $ filter (not . null) lines

-- Parse all symbols from automaton transitions
parseSyms :: Set.Set Trans -> Set.Set Sym
parseSyms transitions = collectSyms (Set.toList transitions) Set.empty
                   where
                       collectSyms [] syms = syms
                       collectSyms (t:rt) syms = Set.unions [Set.singleton (transSym t), collectSyms rt syms]

-- Helper function to perform fixed point iteration
-- takes iteration function, first approximation, and
-- continues until fixed point is found
-- Warn: can loop indefinetely
fixedPointIter :: Eq a => (a->a) -> a -> a
fixedPointIter f x | f x == f (f x)  = f x
                   | otherwise       = fixedPointIter f (f x)

-- Find reachable states
reachableStates :: Fsm -> Set.Set State
-- Using fixed-point iteration with sets
reachableStates fsm = fixedPointIter nextReachableStates Set.empty
    where
        nextReachableStates s | Set.null s = Set.singleton ( start fsm )
                              | otherwise  = Set.union s $ Set.filter isReachable (states fsm)
                                    where isReachable state = any (\t -> transEnd t == state ) (Set.toList (transitions fsm))

-- Find states that from which automaton can properly finish
endableStates :: Fsm -> Set.Set State
endableStates fsm = fixedPointIter nextEndableStates (ends fsm)
    where
        nextTransitions prevStates fsm = Set.filter (\x -> Set.member (transEnd x) prevStates ) (transitions fsm)
        nextEndableStates s = Set.union s (Set.map (\t -> transStart t) (nextTransitions s fsm))

usefulStates :: Fsm -> Set.Set State
usefulStates fsm = Set.intersection (reachableStates fsm) (endableStates fsm)

-- Find transitions that were contain ONLY reachable states
usefulTransitions :: Set.Set State -> Fsm -> Set.Set Trans
usefulTransitions states fsm = Set.fromList ( filter usable $ Set.toList (transitions fsm))
    where
        usable t = (Set.member (transStart t) states) && (Set.member (transEnd t) states)

-- Utility function to unwrap maybes
fromJust = maybe (error "Maybe.fromJust: Nothing") id

-- Get target state using start state, symbol and set of transitions
targetState :: State -> Sym -> Set.Set Trans -> Maybe State
targetState state sym transitions = fmap transEnd firstElem
    where firstElem = find ( \x -> (transStart x) == state && (transSym x == sym) ) transitions

-- Find first item in set that satisfies a predicate
firstByPred :: Ord a => (a -> Bool) -> Set.Set a -> Maybe a
firstByPred pred ekv = find pred ekv

-- Find first set in Set of sets that contains provided item
firstContaining :: Ord a => a -> Set.Set (Set.Set a) -> Maybe (Set.Set a)
firstContaining item ekv = find (\x -> Set.member item x) ekv

-- Find equivalence class using provided equivalence relation for provided item
equivClass :: Ord a => (a->a-> Bool) -> a -> Set.Set a -> Set.Set a
equivClass equiv item domain = Set.filter (\o -> equiv item o) domain

-- Find all equivalence classes of specified domain based on provided equivalence relation
equivClasses :: Ord a => ( a -> a -> Bool) -> Set.Set a -> Set.Set (Set.Set a)
equivClasses equiv domain = Set.map (\v -> equivClass equiv v domain) domain
groupBySets = equivClasses

-- Flattens a set of sets into single set
flattenSets :: Ord a=> Set.Set (Set.Set a) -> Set.Set a
flattenSets sets = Set.unions $ Set.toList sets

-- Perform actual minimalization
minimize :: Fsm -> Fsm
minimize fsm =
    let
        -- Remove unreachable states and states that can't result in accepting of input
        states = usefulStates fsm
        -- Remove invalid transitions
        transitions = usefulTransitions states fsm
        -- Remove unreachable final states
        reachableFinal = Set.intersection states (ends fsm)
        -- Create base approximation of set classes based on simple equivalence (s1 == s1 <=> s1 in final == s2 in final)
        baseEqClasses = groupBySets (\s1 s2 -> (Set.member s1 reachableFinal) == Set.member s2 reachableFinal) states
        -- Next approximation in series is calculated using algorithm from TIN,
        -- We try to find new equivalence classes based on right-congruence (https://groupprops.subwiki.org/wiki/Right_congruence)
        -- 2 state are equivalent if possible edges from these states lead to same equivalence class for all input symbols
        -- s1 == s2 <=> (transition symbol s1) == (transition symbol s2)
        nextEqClasses prevEqClasses = flattenSets $ Set.map (\cls -> groupBySets equiv cls) prevEqClasses
            where
                stateCls state = fromJust $ firstContaining state prevEqClasses
                targetCls state sym = fmap stateCls ( targetState state sym transitions)
                equiv s1 s2 = all (\sym -> targetCls s1 sym == targetCls s2 sym) $ ( Set.toList $ symbols fsm )

        -- Calculate state classes using fixed-point iteration
        minStates = fixedPointIter nextEqClasses baseEqClasses

        stateClasses = Set.toList minStates
        -- Simple conversion from classes to states identified by integers
        classForState item = State $ show (1 + (fromJust $ findIndex (\x -> Set.member item x) stateClasses))
        -- Conversions of invidual parts for the automaton
        minimalStart = classForState (start fsm)
        minimalFinals = Set.map classForState reachableFinal
        minimalTransitions = Set.map mapTransition transitions
            where mapTransition x = Trans {
                    transStart = classForState (transStart x),
                    transSym = transSym x,
                    transEnd = classForState (transEnd x)
                }
    in
    Fsm {
        states = Set.map (\s -> classForState s) states,
        symbols = symbols fsm,
        transitions = minimalTransitions,
        start = minimalStart,
        ends = minimalFinals
    }

-- Read finite state automaton from provided input
parseFsm :: [String] ->  Fsm
parseFsm lines =
    let (statesStr: start: endsStr: rest) = lines
        transitions = parseAllTransitions rest
        symbols = parseSyms transitions
    in Fsm {
        states = parseStateList statesStr,
        transitions = transitions,
        symbols = symbols,
        start = State start,
        ends = parseStateList endsStr
    }

-- Convert state list to string for output
statesToStr :: Set.Set State -> String
statesToStr s = intercalate "," (fmap stateInner (Set.toList s))

-- Convert single transtition to string for output
transitionToString :: Trans -> String
transitionToString (Trans start sym end) = intercalate "," [stateInner start,[symInner sym],stateInner end]

-- Print Automation in standard form
printFsm :: Fsm -> IO ()
printFsm fsm = do
    putStrLn $ statesToStr $ states fsm
    putStrLn $ stateInner $ start fsm
    putStrLn $ statesToStr $ ends fsm
    putStrLn $ intercalate "\n" $ fmap transitionToString $ Set.toList $ transitions fsm

-- Record holding parsed cmdline arguments
data Args = Args {
    inputOnly :: Bool,
    tempOut :: Bool,
    readHandle :: Handle
} deriving(Show)

-- Default value of arguments
defaultArgs = Args {
                inputOnly = False,
                tempOut = False,
                readHandle = stdin
            }

-- List containing configuration records for optparse
options :: [OptDescr (Args -> Args)]
options = [
        Option ['t'] [] (NoArg (\ args -> args { tempOut = True })) "Test",
        Option ['i'] [] (NoArg (\ args -> args { inputOnly = True})) "Input only"
    ]

-- Actual function performing parsing of arguments,
-- source : (https://hackage.haskell.org/package/base-4.10.1.0/docs/System-Console-GetOpt.html)
parseArgs :: [String] -> IO (Args, [String])
parseArgs argv =
  case getOpt Permute options argv of
     (o,n,[]  ) -> return (foldl (flip id) defaultArgs o, n)
     (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))
 where header = "Usage: ic [OPTION...] [Input file]"

-- Open provided file name into handle or do nothing
openFileArg (args,[]) = do
    return args

openFileArg (args,f:rest) = do
    handle <- openFile f ReadMode
    return args { readHandle = handle }


main :: IO ()
main = do {
    parsed <- (getArgs >>= parseArgs >>= openFileArg);
    fsm <- fmap (parseFsm . lines) $ hGetContents (readHandle parsed);

    if inputOnly parsed
        then printFsm fsm;
        else if tempOut parsed
            then printFsm $ minimize fsm;
            else error "You need to provide arguments"

}
