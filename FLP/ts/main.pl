/*
    Author: Michal Hornický <xhorni14@stud.fit.vutbr.cz>
    FLP project #2
*/
/*
 * Input output
*/
read_line(L,C) :-
     get_char(C),
     (is_ending_sym(C), L = [], !;
          read_line(LL,_),% atom_codes(C,[Cd]),
          [C|LL] = L).

is_ending_sym(C) :-
     C == end_of_file;
     (char_code(C,Code), Code==10).

read_lines(Ls) :-
     read_line(L,C),
     ( C == end_of_file, Ls = [] ;
       read_lines(LLs), Ls = [L|LLs]
     ).

write_item(x) :- write(x),write('\n').

write_line([]) :- write('\n').
write_line([Item|NextItems]) :- write(Item),write_line(NextItems).


write_lines([]).
write_lines([Item|NextItems]) :- write_line(Item),write_lines(NextItems).


/* Common list operations */
last([X],X).
last([X|T],F) :- last(T,F).

except_last([X],[]).
except_last([H|T],[H|R]) :- except_last(T,R). 

/* Removes spaces from string representation of rules */
parse_rules([],[]).
parse_rules([H|T],Result) :- 
    [Start,_, Sym, _, Next, _, Act] = H,
    (
        Rule = [Start,Sym,Next,Act],
        parse_rules(T,Tmp),
        Result = [Rule|Tmp]
    ).

/* Prepares tape for execution */
prepare_tape(Tape,Res) :- append(['S'],Tape,Res).

/* Finds next applicable rule */
next_rule([Rule|List],State,Sym,Next,Act) :- 
    [From,Read,To,Do] = Rule,
    (
        From == State, Read == Sym,
        (
            Next = To,
            Act = Do
        );
        next_rule(List,State,Sym, Next,Act)
    ).
    
    
get_sym([], ' ').
get_sym([X|_],X).


/* Parses tape into constituent parts */
parse_tape_inner([],_,[],[],[]).
parse_tape_inner([X],LP,LP,X,RR) :- RR = [' '].
parse_tape_inner([Head|Tape],LPush,Left,State,Right) :- 
    char_type(Head,upper),
        (
            State = Head,
            Left = LPush,
            Right = Tape
        ) ; 
        ( 
            append(LPush, [Head], LP), 
            parse_tape_inner(Tape,LP,IL,IS,IR),
            Left = IL,
            State = IS,
            Right = IR
        )
.

parse_tape(Tape,Left,State,Right) :- parse_tape_inner(Tape,[],Left,State,Right).
/* Performs tape write action */
action_write(Left,NextState,Right,WriteSym,NewTape) :-
(
    [LastSym|RestRight] = Right,
    NewRight = [WriteSym|RestRight],
    append(Left,[NextState],T1),
    append(T1,NewRight,NewTape)
).

/* Moves head to the right */
action_right(Left,NextState,[],NewTape) :- action_right(Left,NextState,' ',NewTape).
action_right(Left,NextState,Right,NewTape) :- 
(
    [Sw|NewRight] = Right,
    append(Left,[Sw],NewLeft),
    append(NewLeft,[NextState],T1),
    append(T1,NewRight,NewTape)
).

/* Moves head left */
action_left(Left,NextState,Right,NewTape) :- 
(
    last(Left,Sw),
    except_last(Left,NewLeft),
    NewRight = [Sw|Right],
    append(NewLeft,[NextState],T1),
    append(T1,NewRight,NewTape)
).
/* Performs one step of turing machine */
execute_step(StartTape,Rules,History) :- 
    parse_tape(StartTape,Left,CurrState,Right),
    [CurrSym|RestRight] = Right,
    (
        CurrState == 'F', !;
        next_rule(Rules,CurrState,CurrSym,NextState,Action),
        (
            Action == 'R', (
                action_right(Left,NextState,Right,NewTape)
            );
            Action == 'L', (
                action_left(Left,NextState,Right,NewTape)
            );
            Action \= 'L', Action \= 'R', (
                action_write(Left,NextState,Right,Action,NewTape)
            )
        ),
        execute_step(NewTape,Rules,InnerHist),
        History = [NewTape|InnerHist]
    )
    .

/* Entrypoint */
start :- prompt(_, ''),
read_lines(LL),
except_last(LL,Str_Rules),
last(LL,Str_tape),
parse_rules(Str_Rules,Rules),
prepare_tape(Str_tape,Tape),
/*
trace(next_rule,+all),
trace(action_left,+all),
trace(action_write,+all),
trace(action_right,+all),
trace(parse_tape,+all),
trace(parse_tape_inner,+all),
*/
execute_step(Tape,Rules,Hist),
write_lines([Tape|Hist]),
halt.
