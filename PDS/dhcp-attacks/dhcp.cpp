#include "dhcp.hpp"

dhcp_msg msg_deserialize(const void *data, usize len) {
    dhcp_msg res = {};
    res.opts = vector<dhcp_opt>{};
    usize msg_fixed_size = offsetof(dhcp_msg, magic) + 4;

    assert(len >= msg_fixed_size);
    memcpy(&res, data, msg_fixed_size);

    res.tid = ntohl(res.tid);
    res.seconds = ntohs(res.seconds);
    res.flags = ntohs(res.flags);
    res.magic = ntohl(res.magic);

    if (res.magic != DHCP_MAGIC) {
        throw logic_error("Not a DHCP packet");
    }

    usize offset = msg_fixed_size;
    while (offset < len) {
        dhcp_opt opt = {};
        opt.type = ((opt_type *) data)[offset++];


        if (opt.type == opt_type::PAD || opt.type == opt_type::END) {
            // Pad or end, length is implicitly 0
        } else {
            opt.len = ((u8 *) data)[offset++];
            for (int i = 0; i < opt.len; i++) {
                opt.data.push_back(((u8 *) data)[offset + i]);
            }
            offset += opt.len;
        }
        res.opts.push_back(opt);

    }

    if (offset != len) {
        throw logic_error("Packet too short");
    }


    return res;
}

void buf_push_u32(vec<u8> &buf, u32 v) {
    union {
        u32 i;
        u8 u[4];
    } _h{};

    _h.i = htonl(v);
    buf.push_back(_h.u[0]);
    buf.push_back(_h.u[1]);
    buf.push_back(_h.u[2]);
    buf.push_back(_h.u[3]);
}

void buf_push_u16(vec<u8> &buf, u16 v) {
    union {
        u16 i;
        u8 u[2];
    } _h{};

    _h.i = htons(v);
    buf.push_back(_h.u[0]);
    buf.push_back(_h.u[1]);
}

void buf_push_ip_addr(vec<u8> &buf, in_addr v) {
    buf_push_u32(buf, htonl(v.s_addr));

}

void buf_push_buf(vec<u8> &buf, const u8 *data, u32 len) {
    for (int i = 0; i < len; ++i) {
        buf.push_back(data[i]);
    }
}

vec<u8> msg_serialize(const dhcp_msg &msg) {
    vec<u8> result;
    u8 tmp[64] = {0};

    result.push_back(msg.op_code);
    result.push_back(msg.hw_type);
    result.push_back(msg.hw_add_len);
    result.push_back(msg.hops);

    buf_push_u32(result, msg.tid);
    buf_push_u16(result, msg.seconds);
    buf_push_u16(result, msg.flags);

    buf_push_ip_addr(result, msg.client_ip_addr);
    buf_push_ip_addr(result, msg.your_ip_addr);
    buf_push_ip_addr(result, msg.server_ip_addr);
    buf_push_ip_addr(result, msg.gateway_ip_addr);
    buf_push_buf(result, msg.client_hw_addr.data, 6);
    buf_push_buf(result, msg._pad, 10);
    buf_push_buf(result, tmp, 64);
    buf_push_buf(result, tmp, 128);
    buf_push_u32(result, DHCP_MAGIC);

    for (auto &opt : msg.opts) {

        if (opt.type == opt_type::PAD || opt.type == opt_type::END) {
            result.push_back((u8) opt.type);
        } else {
            result.push_back((u8) opt.type);
            result.push_back(opt.len);
            buf_push_buf(result, opt.data.data(), opt.data.size());
        }
    }

    return result;
}

const i8 newl = '\n';

ostream &operator<<(ostream &os, const dhcp_msg &msg) {
    os << " op_code: " << (u32) msg.op_code << newl
       << " hw_type: " << (u32) msg.hw_type << newl
       << " hw_add_len: " << (u32) msg.hw_add_len << newl
       << " hops: " << (u32) msg.hops << newl
       << " tid: " << msg.tid << newl
       << " seconds: " << msg.seconds << newl
       << " flags: " << msg.flags << newl
       << " ciaddr: " << ipv4_print(msg.client_ip_addr) << newl
       << " yiaddr: " << ipv4_print(msg.your_ip_addr) << newl
       << " siaddr: " << ipv4_print(msg.server_ip_addr) << newl
       << " giaddr: " << ipv4_print(msg.gateway_ip_addr) << newl
       << " chaddr: " << msg.client_hw_addr << newl
       << " sname: " << msg.sname << newl
       << " bfname: " << msg.bfname << newl
       << " magic: " << msg.magic << endl;
    for (auto &o : msg.opts) {
        os << " opts: " << newl << o;
    }
    return os;
}

ostream &operator<<(ostream &os, const in_addr &ipAddr) {
    os << ipv4_print(ipAddr);
    return os;
}

ostream &operator<<(ostream &os, const mac_addr &addr) {
    os << hex_str(addr.data, 6);
    return os;
}


ostream &operator<<(ostream &os, const dhcp_opt &opt) {
    os << "  type: " << opt_names[(i32) opt.type] << newl
       << "  len: " << (u32) opt.len << newl
       << "  data: " << hex_str(opt.data.data(), opt.len)
       << endl;
    return os;
}
