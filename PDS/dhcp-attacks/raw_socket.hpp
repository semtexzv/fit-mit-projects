//
// Created by semtexzv on 3/4/18.
//

#ifndef DHCP_ATTACKS_SOCK_H
#define DHCP_ATTACKS_SOCK_H

#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <sys/ioctl.h>

#include  "./common.h"
#include "./dhcp.hpp"


/**
 * Implementation of ipv4 checksum function, retrieved from:
 * http://www.microhowto.info/howto/calculate_an_internet_protocol_checksum_in_c.html
 * @param vdata data pointer
 * @param length size of data
 * @return the checksum in network byte order
 */
uint16_t ip_checksum(void *vdata, size_t length) {
    // Cast the data pointer to one that can be indexed.
    char *data = (char *) vdata;

    // Initialise the accumulator.
    uint32_t acc = 0xffff;

    // Handle complete 16-bit blocks.
    for (size_t i = 0; i + 1 < length; i += 2) {
        uint16_t word;
        memcpy(&word, data + i, 2);
        acc += ntohs(word);
        if (acc > 0xffff) {
            acc -= 0xffff;
        }
    }

    // Handle any partial block at the end of the data.
    if (length & 1) {
        uint16_t word = 0;
        memcpy(&word, data + length - 1, 1);
        acc += ntohs(word);
        if (acc > 0xffff) {
            acc -= 0xffff;
        }
    }

    // Return the checksum in network byte order.
    return htons(~acc);
}


/**
 * Enable promiscous mode on network interface
 * Warning: this requires root/ specific capabilities
 * @param sock
 * @param name
 */
inline void enable_promisc_mode(int sock, const string &name) {
    ifreq ifopts = {};
    strncpy(ifopts.ifr_name, name.c_str(), IFNAMSIZ - 1);
    check_error(ioctl(sock, SIOCGIFFLAGS, &ifopts) >= 0, "ioctl: SIOCGIFFLAGS");
    ifopts.ifr_flags |= IFF_PROMISC;
    check_error(ioctl(sock, SIOCSIFFLAGS, &ifopts) >= 0, "ioctl: SIOCSIFFLAGS");
}

/**
 * Helper class containing all addresses in the network stack necessary to send and receive messages using raw_socket
 */
class raw_add {
    mac_addr _mac;
    in_addr _ip;
    u16 _port;
public:

    const u8 *mac_ptr() const {
        return _mac.data;
    }

    const mac_addr &mac() const {
        return _mac;
    }

    const in_addr &ip() const {
        return _ip;
    }

    const u16 &port() const {
        return _port;
    }

    void set_mac(const u8 *v) {
        memcpy(_mac.data, v, LEN_ETH);
    }

    void set_mac(const mac_addr &v) {
        _mac = v;
    }

    void set_ip(const in_addr &v) {
        _ip = v;
    }

    void set_ip_host(u32 v) {
        _ip = in_addr {htonl(v)};
    }

    void set_port(u16 v) {
        _port = v;
    }

    /**
     * Set port, using host representation, this representation is then converted to network-endian
     * @param v
     */
    void set_port_host(u16 v) {
        _port = htons(v);
    }
};

/**
 * Helper structure containing dhcp message with source and destination addresses, used in both sending and receiving
 */
struct addressed_msg {
    raw_add src_add;
    raw_add dst_add;
    dhcp_msg msg;
};

/**
 * Class that wraps sending of messages using SOCK_RAW socket type.
 * SOCK_RAW allows of sending raw ethernet frames, which allows to specify source & destination mac addreses.
 *
 */
class raw_socket {
protected:
    int sendsock;
    int recvsock;
    string if_name;
public:

    explicit raw_socket(const string &iface) : if_name(iface) {
        sendsock = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW);
        check_error(sendsock > 0, "Cant open socket");

        recvsock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
        enable_promisc_mode(recvsock, iface);

        int sockopt;

        check_error(setsockopt(recvsock, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) >= 0, "Set SO_REUSEADDR");
        check_error(setsockopt(recvsock, SOL_SOCKET, SO_BINDTODEVICE, iface.c_str(), IFNAMSIZ - 1) >= 0,
                    "SET_SO_BINDTODEVICE");
    }

    ~raw_socket() {
        close(sendsock);
        close(recvsock);
    }

    /**
     * Get interface IP address. If the interface does nto have assigned ip, returns
     * zero ip address
     */
    in_addr interface_ip() {
        struct ifreq ifr = {};
        strncpy(ifr.ifr_name, if_name.c_str(), IFNAMSIZ - 1);

        if (ioctl(recvsock, SIOCGIFADDR, &ifr) < 0) {
            //ERR("IF IP");
            return in_addr {0};
        }

        return ((sockaddr_in *) &ifr.ifr_addr)->sin_addr;
    }

    /**
     * Get interface index, used internally when sending messages
     */
    i32 interface_index() {
        ifreq if_idx = {};
        strncpy(if_idx.ifr_ifrn.ifrn_name, if_name.c_str(), IFNAMSIZ - 1);
        check_error(ioctl(sendsock, SIOCGIFINDEX, &if_idx) >= 0, "ioctl: SIOCGIFINDEX");
        return if_idx.ifr_ifindex;
    }

    /**
     * Get mac address assigned to the interface, to which this raw_socket is bound
     * @return
     */
    mac_addr interface_mac() {
        mac_addr res{};

        ifreq if_mac = {};
        memset(&if_mac, 0, sizeof(struct ifreq));
        strncpy(if_mac.ifr_name, if_name.c_str(), IFNAMSIZ - 1);
        check_error(ioctl(sendsock, SIOCGIFHWADDR, &if_mac) >= 0, "ioctl: SIOCGIFHWADDR");

        res = (u8 *) if_mac.ifr_hwaddr.sa_data;
        return res;
    }


    /**
     * Receive DHCP message, since this call works on socket in promiscous mode, the socket receives a lot of frames,
     * mac, and port parameters are used when filtering for DHCP messages only.
     * @param mac mac address for filtering incoming messages, full match check is performed
     * @param port udp port for filtering incoming messages
     * @return message with both source and destination addresses
     */
    addressed_msg recv_msg(const mac_addr *mac, u16 port) {
        addressed_msg res{};
        u8 buf[2048] = {0,};
        auto *eh = (ether_header *) buf;
        auto *iph = (ip *) (buf + sizeof(ether_header));
        auto *udph = (udphdr *) (buf + sizeof(ether_header) + sizeof(ip));
        auto headers_size = sizeof(ether_header) + sizeof(ip) + sizeof(udphdr);


        u8 *data_buf = (buf + headers_size);
        sockaddr_ll addr = {0};
        socklen_t len = sizeof(sockaddr_ll);
        ssize_t numbytes;

        auto matches = [&]() -> bool {
            return iph->ip_p == 17
                   && (mac == nullptr || memcmp(eh->ether_dhost, mac->data, LEN_ETH) == 0)
                   && ntohs(udph->dest) == port;
        };
        do {
            numbytes = recvfrom(recvsock, buf, 2048, 0, reinterpret_cast<sockaddr *>(&addr), &len);
        } while (!matches());

        res.src_add.set_mac(eh->ether_shost);
        res.src_add.set_ip(iph->ip_src);
        res.src_add.set_port(udph->source);

        res.dst_add.set_mac(eh->ether_dhost);
        res.dst_add.set_ip(iph->ip_dst);
        res.dst_add.set_port(udph->dest);

        res.msg = msg_deserialize(data_buf, numbytes - headers_size);

        return res;
    }


    /**
     * Send provided dhcp message to destination address, and set proper
     * source addresses, so message can appear that it came from other source than this interface
     * @param msg
     */
    void send_dhcp_msg(const addressed_msg &msg) {
        u8 buf[2048] = {0,};
        auto *eh = (ether_header *) buf;
        auto *iph = (ip *) (buf + sizeof(ether_header));
        auto *udph = (udphdr *) (buf + sizeof(ether_header) + sizeof(ip));
        u8 *data_buf = (buf + sizeof(ether_header) + sizeof(ip) + sizeof(udphdr));

        memcpy(&eh->ether_shost, msg.src_add.mac().data, 6);
        memcpy(&eh->ether_dhost, msg.dst_add.mac().data, 6);

        eh->ether_type = htons(ETH_P_IP);

        iph->ip_hl = 5;
        iph->ip_v = 4;
        iph->ip_tos = 16;
        iph->ip_ttl = 64;
        iph->ip_p = 17; // UDP protocol
        iph->ip_src = msg.src_add.ip();

        iph->ip_dst = msg.dst_add.ip();

        udph->source = msg.src_add.port();
        udph->dest = msg.dst_add.port();

        auto data = msg_serialize(msg.msg);
        memcpy(data_buf, data.data(), data.size());

        iph->ip_len = htons(data.size() + sizeof(udphdr) + sizeof(iphdr));
        iph->ip_sum = 0;
        u16 ip_sum = ip_checksum(iph, sizeof(ip));


        udph->len = htons(data.size() + sizeof(udphdr));
        udph->uh_sum = 0;

        u16 uh_sum = ip_checksum(iph, sizeof(ip) + sizeof(udphdr) + data.size());

        iph->ip_sum = ip_sum;
        //udph->uh_sum = uh_sum;

        auto size = sizeof(ether_header) + sizeof(ip) + sizeof(udphdr) + data.size();

        sockaddr_ll addr = {0};
        addr.sll_family = AF_PACKET;
        addr.sll_ifindex = this->interface_index();
        addr.sll_halen = ETH_ALEN;

        auto sent = sendto(this->sendsock, buf, size, 0, (sockaddr *) &addr, sizeof(sockaddr_ll));

        check_error(sent >= 0, "SEND");
    }

};


#endif //DHCP_ATTACKS_SOCK_H
