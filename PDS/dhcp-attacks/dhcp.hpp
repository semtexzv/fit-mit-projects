//
// Created by semtexzv on 2/14/18.
//

#ifndef DHCP_ATTACKS_DHCP_H
#define DHCP_ATTACKS_DHCP_H

#include <ostream>
#include "common.h"

#define DHCP_MAGIC  0x63825363

#define TYPE_ETH  1
#define LEN_ETH 6
#define FLAG_BROADCAST 0x8000

/**
 * Parse ipv4 address from string,
 * @param add address string
 * @return address in network byte order, contained in in_addr structure
 */
inline in_addr in_addr_parse(const char *add) {
    in_addr addr;
    inet_pton(AF_INET, add, &addr);
    return addr;
}

/**
 * Serialize ipv4 address to string
 * @param add ipv4 address
 * @return string representation of the address
 */
inline string ipv4_print(in_addr add) {
    char str[INET_ADDRSTRLEN];
    struct sockaddr_in sa;
    sa.sin_addr = add;
    sa.sin_family = AF_INET;
    inet_ntop(AF_INET, &(sa.sin_addr), str, INET_ADDRSTRLEN);
    return string(str);
}

/**
 * DHCP message type, used as a value in
 * dhcp_opt with message type of opt_type::DHCP_MESSAGE_TYPE ( 53 )
 */
enum dhcp_msg_type : u8 {
    DHCP_DISCOVER = 1,
    DHCP_OFFER = 2,
    DHCP_REQUEST = 3,
    DHCP_DECLINE = 4,
    DHCP_ACK = 5,
    DHCP_NAK = 6,
    DHCP_RELEASE = 7,
    DHCP_INFORM = 8,
};


/**
 * DHCP/BOOTP option type, this enumeration was automatically generated from official list of options
 */
enum opt_type : u8 {
    PAD = 0,

    SUBNET_MASK = 1,
    TIME_OFFSET = 2,
    ROUTER = 3,
    TIME_SERVER = 4,
    NAME_SERVER = 5,
    DOMAIN_NAME_SERVER = 6,
    LOG_SERVER = 7,
    COOKIE_SERVER = 8,
    LPR_SERVER = 9,
    IMPRESS_SERVER = 10,
    RESOURCE_LOCATION_SERVER = 11,
    HOST_NAME = 12,
    BOOT_FILE_SIZE = 13,
    MERIT_DUMP_FILE = 14,
    DOMAIN_NAME = 15,
    SWAP_SERVER = 16,
    ROOT_PATH = 17,
    EXTENSIONS_PATH = 18,


    IP_FORWARDING = 19,
    NON_LOCAL_SOURCE_ROUTING = 20,
    POLICY_FILTER = 21,
    MAXIMUM_DATAGRAM_REASSEMBLY_SIZE = 22,
    DEFAULT_IP_TIME_TO_LIVE = 23,
    PATH_MTU_AGING_TIMEOUT = 24,
    PATH_MTU_PLATEAU_TABLE = 25,


    INTERFACE_MTU = 26,
    ALL_SUBNETS_ARE_LOCAL = 27,
    BROADCAST_ADDRESS = 28,
    PERFORM_MASK_DISCOVERY = 29,
    MASK_SUPPLIER = 30,
    PERFORM_ROUTER_DISCOVERY = 31,
    ROUTER_SOLICITATION_ADDRESS = 32,
    STATIC_ROUTE = 33,


    TRAILER_ENCAPSULATION = 34,
    ARP_CACHE_TIMEOUT = 35,
    ETHERNET_ENCAPSULATION = 36,


    TCP_DEFAULT_TTL = 37,
    TCP_KEEPALIVE_INTERVAL = 38,
    TCP_KEEPALIVE_GARBAGE = 39,

    NETWORK_INFORMATION_SERVICE_DOMAIN = 40,
    NETWORK_INFORMATION_SERVERS = 41,
    NETWORK_TIME_PROTOCOL_SERVERS = 42,
    VENDOR_SPECIFIC_INFORMATION = 43,
    NETBIOS_OVER_TCP_IP_NAME_SERVER = 44,
    NETBIOS_OVER_TCP_IP_DATAGRAM_DISTRIBUTION_SERVER = 4,
    NETBIOS_OVER_TCP_IP_NODE_TYPE = 46,
    NETBIOS_OVER_TCP_IP_SCOPE = 47,
    X_WINDOW_SYSTEM_FONT_SERVER = 48,
    X_WINDOW_SYSTEM_DISPLAY_MANAGER = 49,
    NETWORK_INFORMATION_SERVICE_PLUS_DOMAIN = 64,
    NETWORK_INFORMATION_SERVICE_PLUS_SERVERS = 65,
    MOBILE_IP_HOME_AGENT = 68,
    SMTP_SERVER = 69,
    POP3_SERVER = 70,
    NNTP_SERVER = 71,
    DEFAULT_WWW_SERVER = 72,
    DEFAULT_FINGER_SERVER = 73,
    DEFAULT_IRC_SERVER = 74,
    STREETTALK_SERVER = 75,
    STREETTALK_DIRECTORY_ASSISTANCE_SERVER = 76,


    REQUESTED_IP_ADDRESS = 50,
    IP_ADDRESS_LEASE_TIME = 51,
    OPTION_OVERLOAD = 52,
    TFTP_SERVER_NAME = 66,
    BOOTFILE_NAME = 67,
    DHCP_MESSAGE_TYPE = 53,
    SERVER_IDENTIFIER = 54,
    PARAMETER_REQUEST_LIST = 55,
    MESSAGE = 56,
    MAXIMUM_DHCP_MESSAGE_SIZE = 57,
    RENEWAL_T1_TIME_VALUE = 58,
    REBINDING_T2_TIME_VALUE = 59,
    VENDOR_CLASS_IDENTIFIER = 60,
    CLIENT_IDENTIFIER = 61,
    END = 255

};
/**
 * Structure containing DHCP option.
 */
typedef struct dhcp_opt {
    opt_type type;
    u8 len = 0;
    vec<u8> data{};

    friend ostream &operator<<(ostream &os, const dhcp_opt &opt);

    dhcp_opt(opt_type type, u8 len, const vec<u8>& data) : type(type), len(len), data(data) {}

    dhcp_opt() {}
} dhcp_opt;

/**
 * DHCP Message. Contains dhcp static fields in line, and a vector of options, that must be individually
 * serialized
 */
typedef struct dhcp_msg {
    u8 op_code;
    u8 hw_type = TYPE_ETH;
    u8 hw_add_len = LEN_ETH;
    u8 hops = 0;
    u32 tid = 0;
    u16 seconds = 0;
    u16 flags = 1;

    in_addr client_ip_addr = in_addr{0};
    in_addr your_ip_addr = in_addr{0};
    in_addr server_ip_addr = in_addr{0};
    in_addr gateway_ip_addr = in_addr{0};

    mac_addr client_hw_addr;
    u8 _pad[10];

    u8 sname[64];
    u8 bfname[128];
    u32 magic = DHCP_MAGIC;

    vector<dhcp_opt> opts;


    /**
     * Utility method to retrieve dhcp message type, opt:type:DHCP_MESSAGE_TYPE ( 53 )
     * @return
     */
    dhcp_msg_type get_dhcp_msg_type() const {
        for (auto &opt : opts) {
            if (opt.type == opt_type::DHCP_MESSAGE_TYPE) {
                return static_cast<dhcp_msg_type>(opt.data[0]);
            }
        }
        throw logic_error("Not a valid DHCP message");
    }

    /**
     * Utility method to retrieve requested dhcp addres, opt_type::REQUESTED_IP_ADDRESS (50)
     * @return requested ip address
     */
    in_addr get_dhcp_requested_addr() const {
        for (auto &opt : opts) {
            if (opt.type == opt_type::REQUESTED_IP_ADDRESS) {
                return *((in_addr *) opt.data.data());
            }
        }
    }

    vec<u8> get_server_id() const {
        for (auto &opt : opts) {
            if (opt.type == opt_type::SERVER_IDENTIFIER) {
                return opt.data;
            }
        }
        return {};
    }

    friend ostream &operator<<(ostream &os, const dhcp_msg &msg);
} dhcp_msg;

/**
 * Utility function to write ip addres into dhcp option buffer
 * @param opt
 * @param addr
 */
inline void fill_addr(dhcp_opt *opt, u32 addr) {

    union {
        unsigned int integer;
        unsigned char byte[4];
    } tmp{};

    tmp.integer = addr;
    opt->len += 4;


    opt->data.push_back(tmp.byte[0]);
    opt->data.push_back(tmp.byte[1]);
    opt->data.push_back(tmp.byte[2]);
    opt->data.push_back(tmp.byte[3]);

}

inline dhcp_opt opt_create_dhcp_msg_type(dhcp_msg_type type) {
    dhcp_opt opt{};
    opt.len = 1;
    opt.data.push_back((u8) type);
    opt.type = opt_type::DHCP_MESSAGE_TYPE;
    return opt;
}

inline dhcp_opt opt_create_subnet_mask(u32 subnet_mask) {
    dhcp_opt opt{};
    opt.type = opt_type::SUBNET_MASK;
    fill_addr(&opt, htonl(subnet_mask));

    return opt;
}

inline dhcp_opt _opt_create_ip(in_addr addr) {
    dhcp_opt opt{};
    fill_addr(&opt, addr.s_addr);
    return opt;
}

inline dhcp_opt opt_create_router(in_addr addr) {
    dhcp_opt opt = _opt_create_ip(addr);
    opt.type = opt_type::ROUTER;

    return opt;
}

inline dhcp_opt opt_create_dns_server(in_addr addr) {
    dhcp_opt opt = _opt_create_ip(addr);
    opt.type = opt_type::DOMAIN_NAME_SERVER;
    return opt;
}

inline dhcp_opt opt_create_domain(string domain) {
    dhcp_opt opt{};
    opt.type = opt_type::DOMAIN_NAME;
    assert(domain.size() < 255);
    opt.len = domain.size();
    for (int i = 0; i < domain.size(); i++) {
        opt.data.push_back(domain[i]);
    }
    return opt;
}

inline dhcp_opt opt_create_param_list(vec<opt_type> params) {
    dhcp_opt opt{};
    opt.type = opt_type::PARAMETER_REQUEST_LIST;
    assert(params.size() < 255);
    opt.len = params.size();
    for (auto &p : params) {
        opt.data.push_back((u8) p);
    }
    return opt;
}

inline dhcp_opt opt_create_max_size(u16 size) {
    dhcp_opt opt{};
    opt.type = opt_type::MAXIMUM_DHCP_MESSAGE_SIZE;
    opt.len = 2;
    opt.data.push_back(0);
    opt.data.push_back(0);

    *((u16 *) opt.data.data()) = htons(size);
    return opt;


}

inline dhcp_opt opt_create_server_id(vec<u8> id) {
    dhcp_opt opt{};
    opt.type = opt_type::SERVER_IDENTIFIER;
    for (auto &a: id) {
        opt.data.push_back(a);
    }
    return opt;
}

inline dhcp_opt opt_create_server_id(in_addr ip) {
    dhcp_opt opt{};
    opt.type = opt_type::SERVER_IDENTIFIER;
    fill_addr(&opt, ip.s_addr);
    return opt;
}

inline dhcp_opt opt_create_ip_address(in_addr ip) {
    dhcp_opt opt{};
    opt.type = opt_type::REQUESTED_IP_ADDRESS;
    fill_addr(&opt, ip.s_addr);
    return opt;

}

inline dhcp_opt opt_create_host_name(string name) {
    dhcp_opt opt{};
    opt.type = opt_type::DOMAIN_NAME;
    assert(name.size() < 255);
    opt.len = (u8) name.size();
    for (int i = 0; i < name.size(); i++) {
        opt.data.push_back((u8) name[i]);
    }
    return opt;
}


inline dhcp_opt opt_create_end() {
    dhcp_opt opt{};
    opt.type = opt_type::END;
    opt.len = 0;
    return opt;
}

/**
 * Deserialize DHCP message from provided buffer
 * @param data
 * @param len
 * @return
 */
dhcp_msg msg_deserialize(const void *data, usize len);

/**
 * Serialize DHCP message into buffer represented as a vector of bytes
 * @param msg
 * @return
 */
vec<u8> msg_serialize(const dhcp_msg &msg);

#endif //DHCP_ATTACKS_DHCP_H
