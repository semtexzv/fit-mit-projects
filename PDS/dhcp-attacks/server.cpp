//
// Created by semtexzv on 2/16/18.
//

#include "server.h"

const vec<opt_descr> options = {
        {"help",       no_argument,       nullptr, 'h', "Print this message"},
        {"interface",  required_argument, nullptr, 'i', "Network interface"},
        {"pool",       required_argument, nullptr, 'p', "Address pool"},
        {"gateway",    required_argument, nullptr, 'g', "Gateway"},
        {"nameserver", required_argument, nullptr, 'n', "Domain name server"},
        {"domain",     required_argument, nullptr, 'd', "Domain name"},
        {"lease",      required_argument, nullptr, 'l', "Duration of lease in seconds"},
};

int main(int argc, char **argv) {

    vec<option> long_opts;
    string short_ops;

    for (auto &o: options) {
        long_opts.push_back(o.opt);
        short_ops.append(o.to_short_str());
    }
    long_opts.push_back({0});


    int c;
    string interface;
    string pool;
    string gateway;
    string dns_str;
    string domain;
    string lease;

    while ((c = getopt_long(argc, argv, short_ops.c_str(), long_opts.data(), nullptr)) > 0) {
        switch (c) {
            case 'h':
                cout << "Usage: ./pds-dhcpstarve <args>" << "\n";
                cout << "Args : " << "\n";
                for (auto &o: options) {
                    cout << "  " << o.help_str() << "\n";
                }
                cout << endl;
                exit(0);
            case 'i':
                interface = optarg;
                break;
            case 'p':
                pool = optarg;
                break;
            case 'g':
                gateway = optarg;
                break;
            case 'n':
                dns_str = optarg;
                break;
            case 'd':
                domain = optarg;
                break;
            case 'l':
                lease = optarg;
                break;
            default:
                break;
        }
    }

    if (interface.empty() || pool.empty() || gateway.empty() || dns_str.empty() || domain.empty() || lease.empty()) {
        cerr << "Missing arguments" << endl;
        abort();
    }

    auto p = pool.find('-');
    in_addr a1 = in_addr_parse(pool.substr(0, p).c_str());
    in_addr a2 = in_addr_parse(pool.substr(p + 1).c_str());
    in_addr dns = in_addr_parse(dns_str.c_str());
    in_addr router = in_addr_parse(gateway.c_str());
    server_opts opts{interface,
                     {
                             a1, a2
                     }, router, dns, domain, stoi(lease)};
    server s{opts};
    s.run();
}

