//
// Created by semtexzv on 2/16/18.
//

#ifndef DHCP_ATTACKS_COMMON_H
#define DHCP_ATTACKS_COMMON_H

#include <vector>
//#include <variant>
#include <sstream>
#include <map>

#include <memory>
#include <cstring>
#include <cassert>


#include <arpa/inet.h>
#include <iostream>
#include <iomanip>
#include <memory>

#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <netdb.h>
#include <net/if.h>

#include <sys/ioctl.h>

#include <getopt.h>

#define  SOCKET_BUFSIZ 4096

#define throw_error(txt) do { \
    using std::stringstream; \
    stringstream ss; \
    ss << (txt) << ":" << string(strerror(errno)) << " @ " << string(__FILE__) << ":" << __LINE__; \
    throw logic_error(ss.str()); \
} while(false);

#define check_error(eval, txt) do { \
    if(!(eval)) { throw_error(txt) } \
} while(false);


using namespace std;

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;
using usize = size_t;

using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;
using isize = ssize_t;


struct opt_descr {
public:
    option opt;
    const char *desc;

    opt_descr(const char *name, int arg, int *flag, int v, const char *descr)
            : opt{name, arg, flag, v}, desc(descr) {
    }

    string to_short_str() const {
        string res;
        res += (char) opt.val;
        if (opt.has_arg == required_argument) {
            res += ":";
        }
        return res;
    }

    string help_str() const {
        stringstream ss;
        string arg = "\t";
        if (opt.has_arg == required_argument) {
            arg = " <v>\t";
        }
        ss << "--" << opt.name << "\t-" <<  (char) opt.val << arg << desc;
        return ss.str();
    }

};

template<typename T>
using vec = std::vector<T>;

template<typename T>
vec<u8> to_bytes(T v);

template<>
inline vec<u8> to_bytes(u16 v) {
    vec<u8> data;
    data.push_back(0);
    data.push_back(0);

    *((u16 *) data.data()) = v;
    return data;
}

template<>
inline vec<u8> to_bytes(u32 v) {
    vec<u8> data;
    data.push_back(0);
    data.push_back(0);
    data.push_back(0);
    data.push_back(0);

    *((u32 *) data.data()) = v;
    return data;
}

constexpr char hexmap[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                           '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

inline std::string hex_str(const u8 *data, int len) {
    std::string s(static_cast<unsigned long>(len * 2), ' ');
    for (int i = 0; i < len; ++i) {
        s[2 * i] = hexmap[(data[i] & 0xF0) >> 4];
        s[2 * i + 1] = hexmap[data[i] & 0x0F];
    }
    return s;
}

/**
 * Contains ethernet MAC address, this class is used to define
 * several common utility functions and operators
 */
typedef struct mac_addr {
    u8 data[6];
public:
    friend ostream &operator<<(ostream &os, const mac_addr &addr);

    mac_addr &operator=(const mac_addr &add) {
        memcpy(data, add.data, 6);
        return *this;
    }

    mac_addr &operator=(const u8 *add) {
        memcpy(data, add, 6);
        return *this;
    }

    /**
     * Generate new random mac address, with correct flags
     * @return generated mac address
     */
    static mac_addr random() {
        mac_addr a{};
        for (int i = 0; i < 6; ++i) {
            a.data[i] = rand();
        }
        a.data[0] &= ~0x11;
        return a;
    }

    bool operator<(const mac_addr &add) const {
        return memcmp(data, add.data, 6) < 0;
    }

    bool operator==(const mac_addr &add) const {
        return memcmp(data, add.data, 6) == 0;
    }
} mac_addr;

/**
 * Simple mac address containing all zeroes
 */
static mac_addr mac_zero = {0};
/**
 * Broadcast mac address
 */
static mac_addr mac_broadcast = {{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}};


const string opt_names[256] = {
        {"Pad"},
        {"Subnet Mask"},
        {"Time Offset"},
        {"Router"},
        {"Time Server"},
        {"Name Server"},
        {"Domain Server"},
        {"Log Server"},
        {"Quotes Server"},
        {"LPR Server"},
        {"Impress Server"},
        {"RLP Server"},
        {"Hostname"},
        {"Boot File Size"},
        {"Merit Dump File"},
        {"Domain Name"},
        {"Swap Server"},
        {"Root Path"},
        {"Extension File"},
        {"Forward On/Off"},
        {"SrcRte On/Off"},
        {"Policy Filter"},
        {"Max DG Assembly"},
        {"Default IP TTL"},
        {"MTU Timeout"},
        {"MTU Plateau"},
        {"MTU Interface"},
        {"MTU Subnet"},
        {"Broadcast Address"},
        {"Mask Discovery"},
        {"Mask Supplier"},
        {"Router Discovery"},
        {"Router Request"},
        {"Static Route"},
        {"Trailers"},
        {"ARP Timeout"},
        {"Ethernet"},
        {"Default TCP TTL"},
        {"Keepalive Time"},
        {"Keepalive Data"},
        {"NIS Domain"},
        {"NIS Servers"},
        {"NTP Servers"},
        {"Vendor Specific"},
        {"NETBIOS Name Srv"},
        {"NETBIOS Dist Srv"},
        {"NETBIOS Node Type"},
        {"NETBIOS Scope"},
        {"X Window Font"},
        {"X Window Manager"},
        {"Address Request"},
        {"Address Time"},
        {"Overload"},
        {"DHCP Msg Type"},
        {"DHCP Server Id"},
        {"Parameter List"},
        {"DHCP Message"},
        {"DHCP Max Msg Size"},
        {"Renewal Time"},
        {"Rebinding Time"},
        {"Class Id"},
        {"Client Id"},
        {"NetWare/IP Domain"},
        {"NetWare/IP Option"},
        {"NIS-Domain-Name"},
        {"NIS-Server-Addr"},
        {"Server-Name"},
        {"Bootfile-Name"},
        {"Home-Agent-Addrs"},
        {"SMTP-Server"},
        {"POP3-Server"},
        {"NNTP-Server"},
        {"WWW-Server"},
        {"Finger-Server"},
        {"IRC-Server"},
        {"StreetTalk-Server"},
        {"STDA-Server"},
        {"User-Class"},
        {"Directory Agent"},
        {"Service Scope"},
        {"Rapid Commit"},
        {"Client FQDN"},
        {"Relay Agent Information"},
        {"iSNS"},
        {"REMOVED/Unassigned"},
        {"NDS Servers"},
        {"NDS Tree Name"},
        {"NDS Context"},
        {"BCMCS Controller Domain Name list"},
        {"BCMCS Controller IPv4 address option"},
        {"Authentication"},
        {"client-last-transaction-time option"},
        {"associated-ip option"},
        {"Client System"},
        {"Client NDI"},
        {"LDAP"},
        {"REMOVED/Unassigned"},
        {"UUID/GUID"},
        {"User-Auth"},
        {"GEOCONF_CIVIC"},
        {"PCode"},
        {"TCode"},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {"REMOVED/Unassigned"},
        {"Unassigned"},
        {"REMOVED/Unassigned"},
        {"Unassigned"},
        {"Netinfo Address"},
        {"Netinfo Tag"},
        {"URL"},
        {"REMOVED/Unassigned"},
        {"Auto-Config"},
        {"Name Service Search"},
        {"Subnet Selection Option"},
        {"Domain Search"},
        {"SIP Servers DHCP Option"},
        {"Classless Static Route Option"},
        {"CCC"},
        {"GeoConf Option"},
        {"V-I Vendor Class"},
        {"V-I Vendor-Specific Information"},
        {"Removed/Unassigned"},
        {"Removed/Unassigned"},
        {"PXE - undefined (vendor specific)"},
        {""},
        {"PXE - undefined (vendor specific)"},
        {""},
        {""},
        {""},
        {"PXE - undefined (vendor specific)"},
        {"PXE - undefined (vendor specific)"},
        {"OPTION_PANA_AGENT"},
        {"OPTION_V4_LOST"},
        {"OPTION_CAPWAP_AC_V4"},
        {"OPTION-IPv4_Address-MoS"},
        {"OPTION-IPv4_FQDN-MoS"},
        {"SIP UA Configuration Service Domains"},
        {"OPTION-IPv4_Address-ANDSF"},
        {""},
        {"GeoLoc"},
        {"FORCERENEW_NONCE_CAPABLE"},
        {"RDNSS Selection"},
        {""},
        {""},
        {""},
        {""},
        {"status-code"},
        {"base-time"},
        {"start-time-of-assign_state"},
        {"query-start-time"},
        {"query-end-time"},
        {"dhcp-assign_state"},
        {"data-source"},
        {"OPTION_V4_PCP_SERVER"},
        {"OPTION_V4_PORTPARAMS"},
        {"DHCP Captive-Portal"},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {"PXELINUX Magic"},
        {"Configuration File"},
        {"Path Prefix"},
        {"Reboot Time"},
        {"OPTION_6RD"},
        {"OPTION_V4_ACCESS_DOMAIN"},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {"Subnet Allocation Option"},
        {"Virtual Subnet Selection (VSS) Option"},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {""},
        {"End"},
};


#endif //DHCP_ATTACKS_COMMON_H
