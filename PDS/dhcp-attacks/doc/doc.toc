\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}DHCP}{2}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Message format}{2}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}DHCP Client State-transitions}{3}{subsection.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4}Attacks}{3}{subsection.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.4.1}DHCP Starvation}{3}{subsubsection.1.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.4.2}Rogue DHCP Server}{3}{subsubsection.1.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.5}Attack mitigation}{3}{subsection.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.5.1}DHCP Starvation}{3}{subsubsection.1.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {1.5.2}Rogue DHCP server}{4}{subsubsection.1.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Project design}{4}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Project attacks}{4}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.1}DHCP Starvation}{4}{subsubsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.2}Rogue DHCP Server}{4}{subsubsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Implementation}{4}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Low level access}{4}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}raw\_socket}{4}{subsubsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Attack implementation}{5}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Implementation environment}{5}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Testing}{6}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Individual attacks}{6}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}DHCP Starvation attack}{6}{subsubsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Rogue DHCP Server attack}{6}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}Results}{6}{subsection.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}DHCP Starvation attack results}{6}{subsection.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}Rogue DHCP server attack}{7}{subsection.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Summary}{7}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Appendices}{8}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}Server log - starvation}{8}{appendix.A}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B}Server log - rogue DHCP server}{10}{appendix.B}
