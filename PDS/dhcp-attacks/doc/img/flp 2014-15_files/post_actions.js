//
// @copyright (c) 2007-2012 fituska
// @license http://opensource.org/licenses/gpl-license.php GNU Public License
//
// PostActions - Like/Dislike posts (supportive functions).
// Topic - reading Topic (supportive functions).
//

var PostActions = {
	//
	// Sends the selected GET request via synchronous AJAX. Returns the response
	// text, or false, if there was an error.
	//
	sendRequestSynchronously : function(request) {
		// AJAX initialization
		var xmlHttp = new XMLHttpRequest();
		if (!xmlHttp) {
			// Just do this (I really don't care about old browsers like IE 6)
			return false;
		}

		// Send the GET request synchronously
		xmlHttp.open('GET', request, false);
		xmlHttp.send(null);
		return xmlHttp.responseText;
	},

	//
	// Like or Dislike post
  //
  // elem - element which called event (check elem.id)
  // PostId  - ID of post
  // LikeElemId - ID of element for Like Count
  // DislikeElemId - ID of element for Dislike Count
  // likeIt - 1 = like; 2 = dislike; 0 = none
  // DisLikeUrl - based URL with post_id=0; (need to parse!)
	//
	// Returns true, if the forum/topic was successfully ignored/unignored, false otherwise.
	//
	LikePost : function(elem, PostId, LikeElemId, DislikeElemId, DisLikeUrl, LikeHrefElemId, DislikeHrefElemId, NoneLikeUser)  {
    // alert(NoneLikeUser);
		var like = document.getElementById(LikeElemId+PostId);
    if(!like)
      return false;
    var like_href = document.getElementById(LikeHrefElemId+PostId);
    if(!like_href)
      return false;
    // alert(like_href.onclick);
    var dislike = document.getElementById(DislikeElemId+PostId);
    if(!dislike)
      return false;
    var dislike_href = document.getElementById(DislikeHrefElemId+PostId);
    if(!dislike_href)
      return false;
    var url = DisLikeUrl.split('?', 2);
    var url2 = url[1].replace('post_id=0', 'post_id='+PostId);
    if(elem.id == LikeHrefElemId+PostId)
      likeIt = 1;
    else
      likeIt = 2;
    // alert(elem.id);
    // alert(LikeHrefElemId+PostId)
    // alert(elem.id == LikeHrefElemId+PostId);
    // alert(likeIt);
    if(likeIt == 1)
    {
      url2 = 'like=1&'+url2;
    }
    else if(likeIt == 2)
    {
      url2 = 'dislike=1&'+url2;
    }
    else if(likeIt == 0)
    {
      url2 = 'like=0&'+url2;
    }
    // alert(like_href.onclick.arguments[0]);
    /*
    alert(typeof like_href.onclick);
    alert(like_href.onclick.toString());
    alert(typeof like_href.onclick);
    alert(like_href.onclick.toString());
    return true;
    */
    var response = PostActions.sendRequestSynchronously(url[0]+'?'+url2);
    // alert(url[0]+'?'+url2+' = '+response);
    // return true;
    // alert(DisLikeUrl + ' = ' + url);
    // return false;
    // if(likeIt == 1)
		// var ignore = (element.innerHTML == ignoreText);
    if(NoneLikeUser)
    {
      var click_href = new Function("event", "return !PostActions.LikePost(this, "+
          PostId+", '"+LikeElemId+"', '"+DislikeElemId+"', '"+DisLikeUrl+"', '"+LikeHrefElemId+
          "', '"+DislikeHrefElemId+"', 0);");
      var click_no = new Function("event", "return false;");
    }

		if (response == '1') {
			// Everything went OK, so change the count of the selected element
      //alert(like.innerHTML);
      //alert(dislike.innerHTML);
			if(likeIt == 1)
      {
        like.innerHTML = parseInt(like.innerHTML) + 1;
        if(!NoneLikeUser)
          dislike.innerHTML = parseInt(dislike.innerHTML) - 1;
        else
        {
          like_href.onclick = click_href;// like_href.onclick.replace('1);', '0);');
          dislike_href.onclick = click_no;
        }
          // alert(like_href)

      }
      else if(likeIt == 2)
      {
        if(!NoneLikeUser)
          like.innerHTML = parseInt(like.innerHTML) - 1;
        else
        {
          like_href.onclick = click_no;
          dislike_href.onclick = click_href;
        }
        //{
        //}
        dislike.innerHTML = parseInt(dislike.innerHTML) + 1;
      }
      var a = like_href.onclick;
      // alert(a);
      like_href.onclick = dislike_href.onclick;
      dislike_href.onclick = a;
			return true;
		}
		else if(response == '21' || response == '20')
    {
      // already checked!
      alert('You change it already!');
      return true;
    }
		else
    {
			// There was some error
			return false;
		}
	}
}

//
// Reading topic
//
var Topic = {
	//
	// Sends the selected GET request via synchronous AJAX. Returns the response
	// text, or false, if there was an error.
	//
	sendRequestSynchronously : function(request) {
		// AJAX initialization
		var xmlHttp = new XMLHttpRequest();
		if (!xmlHttp) {
			// Just do this (I really don't care about old browsers like IE 6)
			return false;
		}

		// Send the GET request synchronously
		xmlHttp.open('GET', request, false);
		xmlHttp.send(null);
		return xmlHttp.responseText;
	},

	//
	// Mark topic as read
	//
	// readText  - text for successfull operation
	// textElementId - ID of element with text
	// readTopicUrl - Url for AJAX "reading" topic
	//
	// Returns true, if the topic was successfully read, false otherwise.
	//
// Topic.readTopic('{L_SEARCH_READ_TOPIC}', 'read_topic_' + {searchresults.TOPIC_ID}, '{searchresults.U_READ_TOPIC}');
	readTopic : function(readText, textElementId, readTopicUrl)  {
		var element = document.getElementById(textElementId);
		// Already clicked, no change
		if(element.innerHTML == readText)
			return true;

		// Asynchronous request
		var xmlHttp = new XMLHttpRequest();
		if (!xmlHttp) {
			// Just do this (I really don't care about old browsers like IE 6)
			return false;
		}

		xmlHttp.open('GET', readTopicUrl, true);
		xmlHttp.onload = function(e) {
			// Everything went OK, so change the text of the selected element
			if(this.status == 200 && this.responseText == '1')
				element.innerHTML = readText;
// 			if(this.status == 200)
// 				alert(this.responseText);
		}

		xmlHttp.onerror = function(e) {
// 			alert(this);
		}

		xmlHttp.send();

/*		var response = Topic.sendRequestSynchronously(readTopicUrl);
		if (response == '1') {
			// Everything went OK, so change the text of the selected element
			element.innerHTML = readText;
			return true;
		} else {
			// There was some error
			return false;
		}*/
		return true;
	}
	// ~ readTopic : function(readText, textElementId, readTopicUrl)  {
}
