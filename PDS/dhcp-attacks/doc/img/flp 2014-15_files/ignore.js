//
// @copyright (c) 2007-2010 fituska
// @license http://opensource.org/licenses/gpl-license.php GNU Public License
//
// Ignore forum/topic (supportive functions).
//

var Ignore = {
	//
	// Sends the selected GET request via synchronous AJAX. Returns the response
	// text, or false, if there was an error.
	//
	sendRequestSynchronously : function(request) {
		// AJAX initialization
		var xmlHttp = new XMLHttpRequest();
		if (!xmlHttp) {
			// Just do this (I really don't care about old browsers like IE 6)
			return false;
		}

		// Send the GET request synchronously
		xmlHttp.open('GET', request, false);
		xmlHttp.send(null);
		return xmlHttp.responseText;
	},

	//
	// Ignores/unignores the selected forum/topic by sending a synchronous HTTP GET
	// request (ignoreUrl/unignoreUrl) to the server and by changing the
	// text of the selected element to unignoreText/ignoreText (ID of this element
	// is textElementId).
	//
	// Returns true, if the forum/topic was successfully ignored/unignored, false otherwise.
	//
	toggleIgnore : function(ignoreText, unignoreText, textElementId, ignoreUrl, unignoreUrl)  {
		var element = document.getElementById(textElementId);
		var ignore = (element.innerHTML == ignoreText);
		var response = Ignore.sendRequestSynchronously(ignore ? ignoreUrl : unignoreUrl);
		if (response == '1') {
			// Everything went OK, so change the text of the selected element
			element.innerHTML = (ignore ? unignoreText : ignoreText);
			return true;
		} else {
			// There was some error
			return false;
		}
	}
}
