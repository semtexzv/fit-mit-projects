//
// @copyright (c) 2007-2010 fituska
// @license http://opensource.org/licenses/gpl-license.php GNU Public License
//
// Phpbb3 [OT] bbcode support.
//

var OffTopic = {
	// Class name of the toggle link
	_otToggleLinkClassName : 'ot-bbcode-toggle-link',
	// Class names of the offtopic element itself (shown and hidden variant)
	_otShownClassName : 'ot-bbcode-shown',
	_otHiddenClassName : 'ot-bbcode-hidden',

	//
	// Returns a list of child nodes with the supplied class name.
	//
	// The code was taken from http://snipplr.com/view.php?codeview&id=1696
	// (I only slightly modified it).
	//
	getElementsByClassName : function(className)  {
		var wantedEls = [];
		var wantedElRe = new RegExp('\\b' + className + '\\b');
		var allEls = document.getElementsByTagName("*");
		for(var i = 0; i < allEls.length; i++) {
			if (wantedElRe.test(allEls[i].className)) {
				wantedEls.push(allEls[i]);
			}
		}

		return wantedEls;
	},

	//
	// Toggles the selected OffTopic.
	//   otElement - OffTopic element to be toggled
	//   shownLinkText - text to be shown when the OffTopic is shown
	//   hiddenLinkText - text to be shown when the OffTopic is hidden
	//
	toggleOffTopic : function (otElement, shownLinkText, hiddenLinkText) {
		var link = otElement.getElementsByTagName('a')[0];
		var hideOtElement = link.innerHTML == shownLinkText;

		// Toggle the element itself
		otElement.className = hideOtElement ? this._otHiddenClassName : this._otShownClassName;

		// Toggle its link text
		link.innerHTML = hideOtElement ? hiddenLinkText : shownLinkText;
	},

	//
	// Toggles all OffTopics on the page.
	//   shownLinkText - text to be shown when OffTopics are shown
	//   shownIconText - icon text to be shown when OffTopics are shown
	//   shownIconPath - icon to be shown when OffTopics are shown
	//   hiddenLinkText - text to be shown when OffTopics are hidden
	//   hiddenIconText - icon text to be shown when OffTopics are hidden
	//   hiddenIconPath - icon to be shown when OffTopics are hidden
	//
	toggleAllOffTopics : function (shownLinkText, shownIconText, shownIconPath,
			hiddenLinkText, hiddenIconText, hiddenIconPath) {
		var hideOtElements = false;

		// Toggle all icons
		// (remember if OffTopic elements are going to be shown or hidden, because
		// the user could hide/shown some of them, which would break the toggling)
		var iconLinks = this.getElementsByClassName(this._otToggleLinkClassName);
		if (iconLinks.length == 0) {
			return;
		}
		for (var i = 0; i < iconLinks.length; i++) {
			var iconImg = iconLinks[i].getElementsByTagName('img')[0];
			if (iconLinks[i].title == shownIconText) {
				iconLinks[i].title = hiddenIconText;
				iconImg.src = hiddenIconPath;
				iconImg.alt = hiddenIconPath;

				hideOtElements = true;
			} else {
				iconLinks[i].title = shownIconText;
				iconImg.src = shownIconPath;
				iconImg.alt = shownIconPath;

				hideOtElements = false;
			}
		}

		// Get all OffTopic elements
		var otElements = this.getElementsByClassName(this._otShownClassName);
		var otElements2 = this.getElementsByClassName(this._otHiddenClassName);
		// Merge these two collections
		for (var i = 0; i < otElements2.length; i++) {
			otElements.push(otElements2[i]);
		}

		// Toggle all OffTopic elements
		for (var i = 0; i < otElements.length; i++) {
			// Toggle the element
			otElements[i].className = hideOtElements ? this._otHiddenClassName : this._otShownClassName;

			// Toggle its link text
			var link = otElements[i].getElementsByTagName('a')[0];
			link.innerHTML = hideOtElements ? hiddenLinkText : shownLinkText;
		}
	}
}
