/**
* phpBB3 forum functions
*/

/**
* Window popup
*/
function popup(url, width, height, name)
{
	if (!name)
	{
		name = '_popup';
	}

	window.open(url.replace(/&amp;/g, '&'), name, 'height=' + height + ',resizable=yes,scrollbars=yes, width=' + width);
	return false;
}

/**
* Jump to page
*/
function jumpto()
{
	var page = prompt(jump_page, on_page);

	if (page !== null && !isNaN(page) && page > 0)
	{
		document.location.href = base_url.replace(/&amp;/g, '&') + '&start=' + ((page - 1) * per_page);
	}
}

/**
* Mark/unmark checklist
* id = ID of parent container, name = name prefix, state = state [true/false]
*/
function marklist(id, name, state)
{
	var parent = document.getElementById(id);
	if (!parent)
	{
		eval('parent = document.' + id);
	}

	if (!parent)
	{
		return;
	}

	var rb = parent.getElementsByTagName('input');

	for (var r = 0; r < rb.length; r++)
	{
		if (rb[r].name.substr(0, name.length) == name)
		{
			rb[r].checked = state;
		}
	}
}

/**
* Resize viewable area for attached image or topic review panel (possibly others to come)
* e = element
*/
function viewableArea(e, itself)
{
	if (!e) return;
	if (!itself)
	{
		e = e.parentNode;
	}

	if (!e.vaHeight)
	{
		// Store viewable area height before changing style to auto
		e.vaHeight = e.offsetHeight;
		e.vaMaxHeight = e.style.maxHeight;
		e.style.height = 'auto';
		e.style.maxHeight = 'none';
		e.style.overflow = 'visible';
	}
	else
	{
		// Restore viewable area height to the default
		e.style.height = e.vaHeight + 'px';
		e.style.overflow = 'auto';
		e.style.maxHeight = e.vaMaxHeight;
		e.vaHeight = false;
	}
}

/**
* Set display of page element
* s[-1,0,1] = hide,toggle display,show
*/
function dE(n, s)
{
	var e = document.getElementById(n);

	if (!s)
	{
		s = (e.style.display == '' || e.style.display == 'block') ? -1 : 1;
	}
	e.style.display = (s == 1) ? 'block' : 'none';
}

/**
* Alternate display of subPanels
*/
function subPanels(p)
{
	var i, e, t;

	if (typeof(p) == 'string')
	{
		show_panel = p;
	}

	for (i = 0; i < panels.length; i++)
	{
		e = document.getElementById(panels[i]);
		t = document.getElementById(panels[i] + '-tab');

		if (e)
		{
			if (panels[i] == show_panel)
			{
				e.style.display = 'block';
				if (t)
				{
					t.className = 'activetab';
				}
			}
			else
			{
				e.style.display = 'none';
				if (t)
				{
					t.className = '';
				}
			}
		}
	}
}

/**
* Call print preview
*/
function printPage()
{
	if (is_ie)
	{
		printPreview();
	}
	else
	{
		window.print();
	}
}

/**
* Show/hide groups of blocks
* c = CSS style name
* e = checkbox element
* t = toggle dispay state (used to show 'grip-show' image in the profile block when hiding the profiles)
*/
function displayBlocks(c, e, t)
{
	var s = (e.checked == true) ?  1 : -1;

	if (t)
	{
		s *= -1;
	}

	var divs = document.getElementsByTagName("DIV");

	for (var d = 0; d < divs.length; d++)
	{
		if (divs[d].className.indexOf(c) == 0)
		{
			divs[d].style.display = (s == 1) ? 'none' : 'block';
		}
	}
}

function selectCode(a)
{
	// Get ID of code block
	var e = a.parentNode.parentNode.getElementsByTagName('DIV')[1];

	// Not IE
	if (window.getSelection)
	{
		var s = window.getSelection();
		// Safari
		if (s.setBaseAndExtent)
		{
			s.setBaseAndExtent(e, 0, e, e.innerText.length - 1);
		}
		// Firefox and Opera
		else
		{
			var r = document.createRange();
			r.selectNodeContents(e);
			s.removeAllRanges();
			s.addRange(r);
		}
	}
	// Some older browsers
	else if (document.getSelection)
	{
		var s = document.getSelection();
		var r = document.createRange();
		r.selectNodeContents(e);
		s.removeAllRanges();
		s.addRange(r);
	}
	// IE
	else if (document.selection)
	{
		var r = document.body.createTextRange();
		r.moveToElementText(e);
		r.select();
	}
}

/**
* Play quicktime file by determining it's width/height
* from the displayed rectangle area
*/
function play_qt_file(obj)
{
	var rectangle = obj.GetRectangle();

	if (rectangle)
	{
		rectangle = rectangle.split(',');
		var x1 = parseInt(rectangle[0]);
		var x2 = parseInt(rectangle[2]);
		var y1 = parseInt(rectangle[1]);
		var y2 = parseInt(rectangle[3]);

		var width = (x1 < 0) ? (x1 * -1) + x2 : x2 - x1;
		var height = (y1 < 0) ? (y1 * -1) + y2 : y2 - y1;
	}
	else
	{
		var width = 200;
		var height = 0;
	}

	obj.width = width;
	obj.height = height + 16;

	obj.SetControllerVisible(true);
	obj.Play();
}

function linenumberOnOff(id){
	var parent = document.getElementById(id);
	var holder = parent.parentNode;

	if (parent.firstChild.nodeName == "OL"){
		var show = 'hide';
	} else if (parent.firstChild.nodeName == "DIV"){
		var show = 'show';
	}

	if (show == 'hide'){
		var child = parent.getElementsByTagName("ol");

		var children = child[0].childNodes;

		var replacement = document.createElement("div");
		replacement.setAttribute("id", id);
		replacement.setAttribute("class", parent.getAttribute("class"));
		replacement.setAttribute("className", parent.getAttribute("className"));
		replacement.setAttribute("style", parent.getAttribute("style"));

		for (var b = 0; b <= children.length - 1; b++){
			if (children[b].nodeType == 1){
				var row = document.createElement("div");

				row.setAttribute("class", children[b].getAttribute("class"));
				row.setAttribute("className", children[b].getAttribute("className"));
				row.setAttribute("style", children[b].getAttribute("style") + "; border-left: none;");
				row.style.cssText = 'border-left: none;';

				var rowdata = children[b].childNodes;

				for (var c = 0; c <= rowdata.length - 1; c++){
					row.appendChild(rowdata[c].cloneNode(true));
				}

				replacement.appendChild(row);
			}
		}

		holder.replaceChild(replacement, parent);
	} else {
		var replacement = document.createElement("div");
		replacement.setAttribute("id", id);
		replacement.setAttribute("class", parent.getAttribute("class"));
		replacement.setAttribute("className", parent.getAttribute("className"));
		replacement.setAttribute("style", parent.getAttribute("style"));

		var list = document.createElement("ol");

		var dds = parent.getElementsByTagName("div");

		for (var b = 0; b <= dds.length -1; b++){
			var row = document.createElement("li");
			row.setAttribute("class", dds[b].getAttribute("class"));
			row.setAttribute("className", dds[b].getAttribute("className"));
			row.setAttribute("style", dds[b].getAttribute('style') + "; border-left: 1px solid #999;");

			var rowdata = dds[b].childNodes;

			for (var c = 0; c <= rowdata.length - 1; c++){
				row.appendChild(rowdata[c].cloneNode(true));
			}

			list.appendChild(row);
		}

		replacement.appendChild(list);
		holder.replaceChild(replacement, parent);
	}
}

function expandCode(id){
	var parent = document.getElementById(id);

	if (parent.style.display === 'block' || parent.style.display === ''){
		parent.style.display = 'none';
	} else {
		parent.style.display = 'block';
	}
}
