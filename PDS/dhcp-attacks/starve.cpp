//
// Created by semtexzv on 2/16/18.
//

#include "starve.h"


const vec<opt_descr> options = {
        {"help",      no_argument,       nullptr,  'h', "Print this message"},
        {"interface", required_argument, nullptr,  'i', "Network interface"},
        {"spoof-mac", no_argument,       nullptr, 's', "Perform mac address spoofing in L2 frames"}
};

int main(int argc, char **argv) {

    vec<option> long_opts;
    string short_ops;

    for (auto &o: options) {
        long_opts.push_back(o.opt);
        short_ops.append(o.to_short_str());
    }
    long_opts.push_back({0});

    int c;
    string interface;
    bool spoof_mac = false;

    while ((c = getopt_long(argc, argv, short_ops.c_str(), long_opts.data(), nullptr)) > 0) {
        switch (c) {
            case 'h':
                cout << "Usage: ./pds-dhcpstarve <args>" << "\n";
                cout << "Args : " << "\n";
                for (auto &o: options) {
                    cout << "  " << o.help_str() << "\n";
                }
                cout << endl;
                exit(0);
            case 'i':
                interface = optarg;
                break;
            case 's':
                spoof_mac = true;
                break;
            default:
                break;
        }

    }

    if (interface.empty()) {
        cerr << "Missing arguments" << endl;
        abort();
    }

    starve app = starve(starve_opts{interface, spoof_mac});
    app.run();

    return 0;

}


