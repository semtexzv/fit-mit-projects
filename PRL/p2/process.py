#!/usr/bin/env python3
import csv
import statistics

import numpy as np
import matplotlib.pyplot as plt

def plot_file(fname,slabel):
    csvfile = open(fname)
    data = csv.reader(csvfile)
    vals = {}
    for line in data:
        k = int(line[0])
        v = float(line[1])
        if k not in vals:
            vals[k] = []

        vals[k].append(v)

    avgs = {}
    devs = {}
    for k,meas in vals.items():
        avgs[k] = statistics.mean(meas)
        devs[k] = statistics.pstdev(meas)


    t = np.linspace(0, 900000,1000)


    c1 = plt.errorbar(vals.keys(),
        list(avgs.values()),
        yerr=list(devs.values()),
        fmt='-o',label=slabel,
        capsize=3,alpha=0.8,linestyle='dashed',linewidth=1)

plot_file("p1.csv","1 processor")
plot_file("p2.csv","2 processors")
plot_file("p3.csv","3 processors")
plot_file("p4.csv","4 processors")
#plot_file("p8.csv","8 processors")

plt.legend()

plt.xlabel('# of items')
plt.ylabel('Time [s]')
plt.grid(linestyle='-',which='both')
plt.savefig('plot.png')

plt.show()
