#!/bin/bash

values=${1:-13}
procs=${2:-3}

outname=sort

#preklad cpp zdrojaku
mpic++ --prefix /usr/local/share/OpenMPI -o $outname mss.cpp

#vyrobeni souboru s random cisly
dd if=/dev/random bs=1 count=$values of=numbers 2> /dev/null

#spusteni
mpirun --prefix /usr/local/share/OpenMPI -oversubscribe -np $procs ./$outname $values


#rm -rf numbers
