#!/usr/bin/env bash

outname=sort

#preklad cpp zdrojaku
mpic++ --prefix /usr/local/share/OpenMPI -DBENCH -o $outname mss.cpp

for procs in {1..4} 
  do
    NAME="p${procs}".csv
    rm -rf $NAME
    touch $NAME
    for size in {100000..1000000..100000}
      do
          dd if=/dev/random bs=1 count=$size of=numbers
          for iter in {0..5}
            do
              echo -n "${size}," >> $NAME
              duration=$(/usr/bin/time -o $NAME -f %e -a mpirun --prefix /usr/local/share/OpenMPI -oversubscribe -np $procs ./sort $size > /dev/null)
          done
      done
done
