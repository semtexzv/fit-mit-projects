#include <iostream>
#include <fstream>
#include <mpi.h>
#include <algorithm>
#include <sstream>
#include <assert.h>

using namespace std;

#define TAG 0

/*
int values_for_proc(int j, int P, int N) {
    return N * (j + 1) / P - int(N * j / float(P));
}
 */
template<typename T>
struct array_deleter {
    void operator()(T const *p) {
        delete[] p;
    }
};

int values_for_proc(int proc_idx, int proc_count, int val_count) {
    float precise_fract = val_count / (float) proc_count;
    int vals_per_proc = val_count / proc_count;

    if (proc_idx == proc_count - 1) {
        return val_count - (vals_per_proc * proc_idx);
    }

    return vals_per_proc;
}

int main(int argc, char **argv) {

    int input_size = stoi(argv[1]);

    int proc_idx, proc_count;
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &proc_idx);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_count);

    int c = 0;
    for (int j = 0; j < proc_count; ++j) {
        c += values_for_proc(j, proc_count, input_size);
    }
    assert(input_size == c);

    bool procs_count_even = proc_count % 2 == 0;

    int vals_for_current = values_for_proc(proc_idx, proc_count, input_size);
    int max_vals = values_for_proc(proc_count - 1, proc_count, input_size);
    int data[vals_for_current];


    if (proc_idx == 0) {
        int tmp[max_vals];
        ifstream input("./numbers", ios::in | ios::binary);
        for (int p = 0; p < proc_count; p++) {
            int actual_vals = 0;
            for (actual_vals = 0; actual_vals < values_for_proc(p, proc_count, input_size); actual_vals++) {
                if (!input.good()) {
                    break;
                }

                tmp[actual_vals] = input.get();
                #ifndef BENCH
                cout << (int) tmp[actual_vals] << " ";
                #endif
            }
            if (p == 0) {
                memcpy(data, tmp, actual_vals * sizeof(int));
            } else {
                MPI_Send(&tmp, actual_vals, MPI_INT, p, TAG, MPI_COMM_WORLD);
            }
        }
        cout << endl;
    }

    MPI_Status stat;

    if (proc_idx != 0) {
        MPI_Recv(&data, vals_for_current, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat);
    }

    sort(data, data + vals_for_current);


    int cycles = proc_count;
    for (int k = 1; k <= cycles; k++) {
        bool step_even = k % 2 == 0;
        bool step_odd = !step_even;

        bool even = proc_idx % 2 == 0;
        bool odd = !even;

        bool is_receiving = step_odd && even || step_even && odd;
        bool is_sending = step_odd && odd || step_even && even;

        bool first = proc_idx == 0;
        bool last = proc_idx == proc_count - 1;


        int distrib_dest = proc_idx - 1;
        int collect_dest = proc_idx + 1;

        bool is_disabled =
                (step_even && first)
                || (step_odd && last && !procs_count_even)
                || (step_even && last && procs_count_even);

        if (!is_disabled) {
            assert(is_sending != is_receiving);
            if (is_sending) {
                MPI_Send(data, vals_for_current, MPI_INT, distrib_dest, TAG, MPI_COMM_WORLD);
                // Wait for data
                MPI_Recv(data, vals_for_current, MPI_INT, distrib_dest, TAG, MPI_COMM_WORLD, &stat);

            }

            if (is_receiving) {

                int recv_count = values_for_proc(collect_dest, proc_count, input_size);
                int recv_buf[recv_count];

                int merge_buf[vals_for_current + recv_count];

                MPI_Recv(recv_buf, recv_count, MPI_INT, collect_dest, TAG, MPI_COMM_WORLD, &stat);


                merge(recv_buf, recv_buf + recv_count, data, data + vals_for_current, merge_buf);

                MPI_Send(merge_buf + vals_for_current, recv_count, MPI_INT, collect_dest, TAG, MPI_COMM_WORLD);

                for (int i = 0; i < vals_for_current; ++i) {
                    data[i] = merge_buf[i];
                }


            }
        }
    }


    if (proc_idx != 0) {
        MPI_Send(data, vals_for_current, MPI_INT, 0, TAG, MPI_COMM_WORLD);
    }


    if (proc_idx == 0) {
        for (int p = 0; p < proc_count; p++) {
            int val_count = values_for_proc(p, proc_count, input_size);
            int vals[val_count];
            if (p == 0) {
                memcpy(vals, data, val_count * sizeof(int));
            } else {
                MPI_Recv(vals, val_count, MPI_INT, p, TAG, MPI_COMM_WORLD, &stat);
            }
            
            #ifndef BENCH
            for (int i = 0; i < val_count; i++) {
                cout << (int) vals[i] << endl;
            }
            #endif


        }
    }

    MPI_Finalize();

    return 0;
}