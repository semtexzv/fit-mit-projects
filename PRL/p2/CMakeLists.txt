cmake_minimum_required(VERSION 3.9)
project(mpitest)

set(CMAKE_CXX_STANDARD 11)
find_package(MPI REQUIRED)

set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS})
include_directories(MPI_INCLUDE_PATH)

add_executable(mpitest main.cpp)
target_link_libraries(mpitest ${MPI_LIBRARIES})