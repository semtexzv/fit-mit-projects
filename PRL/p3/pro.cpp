#include <iostream>
#include <mpi.h>
#include <vector>
#include <fstream>
#include <cmath>
#include <iomanip>

#define BEGIN_PAR(X) for(int i =0; i < (X) ; i++) {
#define END_PAR }
#define TAG 0
using namespace std;

int lchild_vertex(int parent) {
    return parent * 2 + 1;
}

int rchild_vertex(int parent) {
    return parent * 2 + 2;
}

int parent_vertex(int child) {
    return (child - 1) / 2;
}

/**
 * Utility function to calculate index of first edge in adjecency list for vertex i
 * @param nodes number of vertices in tree
 * @param i index of vertex
 * @return index of first edge in adjecency list
 */
int adj_list_begin(int nodes, int i) {
    if (i == 0) {
        return 0;
    }
    return 2 * i - 1;
}

/**
 * Find ending index in linked list
 */
int find_list_end(const int *list, int count) {
    for (int i = 0; i < count; ++i) {
        if (list[i] == i) {
            return i;
        }
    }
    return -1;
}

/**
 * Find specific element in an array
 */
int find_list_elem(const int *list, int count, int elem) {
    for (int i = 0; i < count; ++i) {
        if (list[i] == elem) {
            return i;
        }
    }
    return -1;
}

/**
 * Utility function to find n predecessors in linked list
 */
int find_iter_pred(int *list, int count, int elem, int n) {
    int pred = elem;
    for (int i = 0; i < n; ++i) {
        pred = find_list_elem(list, count, pred);
    }
    return pred;
}

/**
 * Perform parallel suffix sum
 * @param list_elem element of linked list for this processor
 * @param w weight or rank for this processor
 * @return suffix sum for current processor
 */
int suffix_sum(int list_elem, int weight) {
    int idx, proc_count;
    MPI_Comm_rank(MPI_COMM_WORLD, &idx);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_count);

    int succ = list_elem;
    int pred = -1;
    int last = 3;

    if (succ != idx) {
        MPI_Send(&idx, 1, MPI_INT, succ, TAG, MPI_COMM_WORLD);
    }

    if (idx != 0) {
        MPI_Recv(&pred, 1, MPI_INT, MPI_ANY_SOURCE, TAG, MPI_COMM_WORLD, nullptr);
    }
    MPI_Barrier(MPI_COMM_WORLD);

    for (int k = 0; k < ceil(log2(proc_count)); k++) {

        int pred_pred = -1;
        int succ_succ = -1;
        int succ_weight = 0;

        //cout << idx << " PPP " << pred << endl;
        if (pred >= 0 && idx != last) {
            MPI_Send(&weight, 1, MPI_INT, pred, TAG, MPI_COMM_WORLD);
            MPI_Send(&succ, 1, MPI_INT, pred, TAG, MPI_COMM_WORLD);
        }
        MPI_Barrier(MPI_COMM_WORLD);
        if (succ >= 0 && succ != last) {
            MPI_Recv(&succ_weight, 1, MPI_INT, succ, TAG, MPI_COMM_WORLD, nullptr);
            MPI_Recv(&succ_succ, 1, MPI_INT, succ, TAG, MPI_COMM_WORLD, nullptr);
        }

        if (succ >= 0 && succ != last) {
            MPI_Send(&pred, 1, MPI_INT, succ, TAG, MPI_COMM_WORLD);
        }

        if (pred >= 0 && idx != last) {
            MPI_Recv(&pred_pred, 1, MPI_INT, pred, TAG, MPI_COMM_WORLD, nullptr);
        }

        //cout << k << " " << idx << " p " << setw(2) << pred << "/" << setw(2) << pred_pred << " s " << succ << "/"             << succ_succ << endl;
        pred = pred_pred;
        succ = succ_succ;
        weight += succ_weight;

        MPI_Barrier(MPI_COMM_WORLD);

    }
    return weight;
}

int main(int argc, char **argv) {

    int idx, proc_count;
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &idx);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_count);


    int vertices = (proc_count + 2) / 2;
    int vertices_list = (vertices + 1) / 2;
    int vertices_nonlist = vertices - vertices_list;

    int forward = idx % 2 == 0;
    int vertex_down = (forward ? idx + 2 : idx + 1) / 2;

    int vertex_up = parent_vertex(vertex_down);

    int start = forward ? vertex_up : vertex_down;
    int end = forward ? vertex_down : vertex_up;

    int inverse_idx = forward ? idx + 1 : idx - 1;
    int next = -1;

    if (idx % 4 == 0) {
        next = idx + 2;
    } else if (vertex_down < vertices_nonlist && idx % 2 == 1) {
        next = 2 * idx + 2;
    }

    if (next >= proc_count) {
        next = -1;
    }

    int next_rev = -1;
    // DIstribute next(er)
    MPI_Send(&next, 1, MPI_INT, inverse_idx, TAG, MPI_COMM_WORLD);
    MPI_Recv(&next_rev, 1, MPI_INT, inverse_idx, TAG, MPI_COMM_WORLD, nullptr);

    // Create etour
    int etour = -1;
    if (next_rev >= 0) {
        etour = next_rev;
    } else {
        etour = adj_list_begin(vertices, end);
    }

    // LOOP ON LAST ITEM
    if (etour == 0) {
        etour = idx;
    }

    int weight = 0;
    if (forward) {
        weight = 1;
    }
    weight = suffix_sum(etour, weight);

    // cout << idx << " == " << start << "->" << end << " E " << etour << " W " << weight << endl;

    int preorder = -1;
    if (forward) {
        preorder = vertices - weight;
    }

    MPI_Barrier(MPI_COMM_WORLD);


    int po[proc_count];
    int ee[proc_count];

    MPI_Gather(&preorder, 1, MPI_INT, &po, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(&end, 1, MPI_INT, &ee, 1, MPI_INT, 0, MPI_COMM_WORLD);

    int zero_pos = find_list_elem(ee, proc_count, 0);
    po[zero_pos] = 0;


    if (idx == 0) {
        for (int i = 0; i < vertices; ++i) {
            int edge_pos = find_list_elem(po, proc_count, i);
            if (edge_pos >= 0) {
                int vert_pos = ee[edge_pos];
                cout << argv[1][vert_pos];
            }
        }
        cout << endl;
    }




    /*
    edge edg;



    int vertex_firsts[vertices];
    int inverse_idx = -1;
    int next_idx = -1;
    int next_inverse_idx = -1;

    if (idx == 0) {
        adj_list_t adj_list = prepare_tree(argv[1]);
        for (int v = 0; v < vertices; v++) {
            vertex_firsts[v] = adj_list.edge_lists[v][0].normal;
        }
        for (int p = 0; p < proc_count; p++) {
            if (p == 0) {
                edg = adj_list.edges[p];
                inverse_idx = adj_list.inverses[p];
                next_idx = adj_list.nexts[p];
                next_inverse_idx = adj_list.nexts[adj_list.inverses[p]];
            } else {
                MPI_Send(vertex_firsts, vertices, MPI_INT, p, TAG, MPI_COMM_WORLD);
                MPI_Send(&adj_list.edges[p], 2, MPI_INT, p, TAG, MPI_COMM_WORLD);
                MPI_Send(&adj_list.inverses[p], 1, MPI_INT, p, TAG, MPI_COMM_WORLD);
                MPI_Send(&adj_list.nexts[p], 1, MPI_INT, p, TAG, MPI_COMM_WORLD);
                MPI_Send(&adj_list.nexts[adj_list.inverses[p]], 1, MPI_INT, p, TAG, MPI_COMM_WORLD);
            }
        }
    } else {
        MPI_Recv(vertex_firsts, vertices, MPI_INT, 0, TAG, MPI_COMM_WORLD, nullptr);
        MPI_Recv(&edg, 2, MPI_INT, 0, TAG, MPI_COMM_WORLD, nullptr);
        MPI_Recv(&inverse_idx, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, nullptr);
        MPI_Recv(&next_idx, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, nullptr);
        MPI_Recv(&next_inverse_idx, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, nullptr);
    }

    // Create ETOUR
    int etour = -1;
    if (next_inverse_idx >= 0) {
        etour = next_inverse_idx;
    } else {
        etour = vertex_firsts[edg.end];
    }
    // MAKE ROOT
    if (etour == 0) {
        etour = idx;
    }

    int etour_all[proc_count] = {0};

    MPI_Send(&etour, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD);
    if (idx == 0) {
        for (int i = 0; i < proc_count; i++) {
            MPI_Recv(&etour_all[i], 1, MPI_INT, i, TAG, MPI_COMM_WORLD, nullptr);
        }
        for (int i = 0; i < proc_count; i++) {
            MPI_Send(&etour_all, proc_count, MPI_INT, i, TAG, MPI_COMM_WORLD);
        }
    }
    MPI_Recv(&etour_all, proc_count, MPI_INT, 0, TAG, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);
    int rank = list_rank(etour);
    MPI_Barrier(MPI_COMM_WORLD);
    int posn = proc_count - rank;
    int inverse_posn = 0;

    MPI_Send(&posn, 1, MPI_INT, inverse_idx, TAG, MPI_COMM_WORLD);
    MPI_Recv(&inverse_posn, 1, MPI_INT, inverse_idx, TAG, MPI_COMM_WORLD, nullptr);
    bool is_forward = posn < inverse_posn;
    int weight = is_forward ? 1 : 0;
    // SUFFIX SUMS(Etour,Weight)


    cout << idx << "\tn " << rank << endl;

     */
    MPI_Finalize();
}