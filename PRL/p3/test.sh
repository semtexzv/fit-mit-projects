#!/bin/bash

TREE=${1:-"ABCDEFG"}
PROCS=$(printf $TREE | wc -m)
PROCS=$(( ($PROCS * 2) - 2 ))
outname=pro

#preklad cpp zdrojaku
mpic++ --prefix /usr/local/share/OpenMPI -o $outname pro.cpp


#spusteni
mpirun --prefix /usr/local/share/OpenMPI -oversubscribe -np $PROCS ./$outname $TREE