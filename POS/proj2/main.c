#define _POSIX_SOURCE 1
#define _POSIX_C_SOURCE 1
//#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <sys/wait.h>
#include <string.h>
#include <stdbool.h>

#define PIPE_READ 0
#define PIPE_WRITE 1

#define INPUT_TOTAL 513
#define INPUT_SIZE 512

#define MAX_CMD_ARGS 64
#define MAX_PIPED_CMDS 64
#define MAX_BG_JOBS 64

pid_t pid_list[MAX_PIPED_CMDS];
pid_t bg_jobs[MAX_BG_JOBS];

char input_buf[INPUT_TOTAL];
ssize_t input_size = 0;

struct monitor {
    pthread_mutex_t mutex;
    pthread_cond_t cond_var;

    int (*cond_check)(void);
};

void monitor_wait(struct monitor *mon) {
    pthread_mutex_lock(&mon->mutex);
    while (mon->cond_check() == 0) {
        pthread_cond_wait(&mon->cond_var, &mon->mutex);
    }
    pthread_mutex_unlock(&mon->mutex);
}

void monitor_signal(struct monitor *mon) {
    pthread_mutex_lock(&mon->mutex);
    pthread_cond_signal(&mon->cond_var);
    pthread_mutex_unlock(&mon->mutex);
}

#define MONITOR_INITITIALIZER(cond) {PTHREAD_MUTEX_INITIALIZER, PTHREAD_COND_INITIALIZER, cond}

int input_rdy() {
    return input_size > 1;
}

int use_rdy() {
    return input_size == 0;
}

struct monitor read_monitor = MONITOR_INITITIALIZER(&input_rdy);
struct monitor use_monitor = MONITOR_INITITIALIZER(&use_rdy);

void *input_thread_fun(void *args) {
    while (1) {
        printf("$ ");
        fflush(stdout);

        input_size = read(STDIN_FILENO, input_buf, INPUT_TOTAL);
        if (input_size >= INPUT_TOTAL) {
            fprintf(stderr, "Input too long\n");
            while (strchr(input_buf, '\n') == NULL) {
                input_size = read(STDIN_FILENO, input_buf, 1);
            }

        } else {
            input_buf[input_size - 1] = 0;
            if (input_size < 0) {
                perror("Could load data from stdin");
            }

            if (input_rdy()) {
                monitor_signal(&read_monitor);
                monitor_wait(&use_monitor);
            }
        }
    }
}


void nullify_starting_spaces(char **line) {
    while (**line == ' ') {
        **line = 0;
        (*line)++;
    }
}

void nullify_trailing_spaces(char *line) {
    if (line != NULL) {
        char *pos = NULL;

        while ((pos = strrchr(line, ' ')) != NULL) {
            *pos = 0;
        }
    }
}

bool builtin_cd(char **argv) {
    if (argv[1] == NULL) {
        argv[1] = getenv("HOME");
    }
    if (argv[1] != NULL) {
        if (chdir(argv[1])) {
            perror("Could not change dir");
        }
    }
    return true;
}

bool builtin_exit(char **argv) {
    int code = 0;
    if (argv[1] != NULL) {
        if (chdir(argv[1])) {
            code = (int) strtol(argv[1], NULL, 10);
        }
    }
    _exit(code);
    return true;
}

bool builtin_help(char **argv) {
    printf("No help here, TODO\n");
    return true;
}

struct builtin {
    const char *name;

    bool (*fun)(char **argv);
};

bool try_exec_builtins(char **argv) {
    static struct builtin builtins[] = {
            {"cd",   builtin_cd},
            {"exit", builtin_exit},
            {"help", builtin_help},
            {"?",    builtin_help},
            {NULL, NULL}
    };

    for (int i = 0; builtins[i].name != NULL; ++i) {
        if (strcmp(builtins[i].name, argv[0]) == 0) {
            return builtins[i].fun(argv);
        }
    }

    return false;
}

void process_line(char *line) {
    char *bg = strchr(line, '&');
    int is_bg = bg != 0;
    if (is_bg) {
        *bg = 0;
    }

    char *in_sep_pos = strrchr(line, '<');
    char *out_sep_pos = strrchr(line, '>');

    if (in_sep_pos != NULL) {
        *in_sep_pos = 0;
        in_sep_pos += 1;
        nullify_starting_spaces(&in_sep_pos);
        printf("in : %s\n", in_sep_pos);
    }

    if (out_sep_pos != NULL) {
        *out_sep_pos = 0;
        out_sep_pos += 1;
        nullify_starting_spaces(&out_sep_pos);
        printf("out: %s\n", out_sep_pos);
    }

    nullify_trailing_spaces(out_sep_pos);
    nullify_trailing_spaces(in_sep_pos);

    char *commands[MAX_PIPED_CMDS];
    size_t command_count = 0;

    for (char *pipe = line; pipe != NULL; pipe = strchr(pipe, '|')) {
        if (*pipe == '|') {
            *pipe = '\0';
            pipe += 1;
        }
        nullify_starting_spaces(&pipe);
        commands[command_count++] = pipe;
    }

    int last_pipe_endpoint = -1;
    for (int i = 0; i < command_count; ++i) {
        char *cmd_argvs[MAX_CMD_ARGS];
        size_t cmd_argc = 0;

        char *cmd_begin = commands[i];

        for (char *arg = cmd_begin; arg != NULL; arg = strchr(arg, ' ')) {
            if (*arg == ' ') {
                *arg = '\0';
                arg += 1;
            }

            if (*arg != '\0') {
                nullify_starting_spaces(&arg);
                cmd_argvs[cmd_argc++] = arg;
            }
            cmd_argvs[cmd_argc] = NULL;
        }


        if (try_exec_builtins(cmd_argvs)) {
            return;
        }

        int cmd_first = i == 0;
        int cmd_last = i == command_count - 1;

        int fd[2];

        int current_pipe_in = last_pipe_endpoint;
        int current_pipe_out = -1;

        if (!cmd_last || is_bg) {
            pipe(fd);

            current_pipe_out = fd[PIPE_WRITE];
            last_pipe_endpoint = fd[PIPE_READ];
        }

        int pid = fork();
        if (pid == 0) {
            if (cmd_first) {
                if (in_sep_pos != NULL) {
                    FILE *in = fopen(in_sep_pos, "r");
                    assert(in != NULL);
                    dup2(fileno(in), STDIN_FILENO);
                    fclose(in);
                }
            } else {
                dup2(current_pipe_in, STDIN_FILENO);
                close(current_pipe_in);
            }
            if (cmd_last && out_sep_pos != NULL) {
                FILE *out = fopen(out_sep_pos, "w");
                assert(out != NULL);
                dup2(fileno(out), STDOUT_FILENO);
                fclose(out);

            } else if (!cmd_last || is_bg) {
                dup2(current_pipe_out, STDOUT_FILENO);
                close(current_pipe_out);
            }
            if (execvp(cmd_argvs[0], cmd_argvs) != 0) {
                perror("Failed to execute");
                abort();
            }
        } else {
            pid_list[i] = pid;
            if (current_pipe_out != -1) {
                close(current_pipe_out);
            }

            if (cmd_last) {
                close(last_pipe_endpoint);
            }
        }
    }

    if (is_bg) {
        for (int i = 0; i < MAX_BG_JOBS; ++i) {
            if (bg_jobs[i] == 0) {
                bg_jobs[i] = pid_list[command_count - 1];
                printf("JOB [%d] running with pid:%d\n", i, bg_jobs[i]);
                break;
            }
        }
    }
    if (!is_bg) {
        for (int i = 0; i < command_count; ++i) {
            waitpid(pid_list[i], NULL, 0);
            pid_list[i] = 0;
        }
    }
}

void *worker_thread_fun(void *_unused) {
    while (1) {
        monitor_wait(&read_monitor);

        process_line(input_buf);

        input_size = 0;
        memset(input_buf, 0, INPUT_TOTAL);
        monitor_signal(&use_monitor);
    }
}

void sigint_handler(int sig) {
    if (sig == SIGINT) {
        int terminated = 0;
        for (int i = 0; i < MAX_PIPED_CMDS; ++i) {
            if (pid_list[i] != 0) {
                terminated += 1;
                kill(pid_list[i], SIGTERM);
            }
        }
    } else if (sig == SIGCHLD) {
        pid_t pid;
        int status;
        while ((pid = wait(&status)) != -1) {
            for (int i = 0; i < MAX_BG_JOBS; ++i) {
                if (bg_jobs[i] == pid) {
                    bg_jobs[i] = 0;
                    printf("JOB [%d](pid:%d) exited with code : %d \n", i, pid, WEXITSTATUS(status));
                    fflush(stdout);
                    break;
                }
            }
        }
    }
}


int main() {
    signal(SIGINT, &sigint_handler);
    signal(SIGCHLD, &sigint_handler);

    pthread_t input_thread;
    if (pthread_create(&input_thread, NULL, input_thread_fun, NULL) != 0) {
        perror("Could not create input thread");
    }

    pthread_t exec_thread;
    if (pthread_create(&exec_thread, NULL, worker_thread_fun, NULL) != 0) {
        perror("Could not create worker thread");
    }
    pthread_join(input_thread, NULL);
    pthread_join(exec_thread, NULL);
    return 0;


}