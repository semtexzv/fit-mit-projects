// Author : Michal Hornický <xhorni14@fit.vutbr.cz>
#define _XOPEN_SOURCE
#define _XOPEN_SOURCE_EXTENDED 1 /* XPG 4.2 - needed for WCOREDUMP() */

#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <unistd.h>
#include <assert.h>

void print_self_info(const char *label) {
    printf("%s  identification: \n", label); /*grandparent/parent/child */
    printf("	pid  = %d,	ppid = %d,	pgrp = %d\n", getpid(), getppid(), getpgrp());
    printf("	uid  = %d,	gid  = %d\n", getuid(), getgid());
    printf("	euid = %d,	egid = %d\n", geteuid(), getegid());
}

char *get_level_label(int level) {
    switch (level) {
        case 0:
            return "grandparent";
        case 1:
            return "parent";
        case 2:
            return "child";
        default:
            return "unknown";
    }
}

void run_child(int level, int argc, char **argv) {
    char *label = get_level_label(level);
    print_self_info(label);

    if (level >= 2) {
        assert(execv(argv[1], argv + 1) >= 0);
    }

    int child_pid = fork();
    assert(child_pid >= 0);

    if (child_pid == 0 && level < 2) {
        run_child(level + 1, argc, argv);
        return;
    }

    int status = 0;
    assert(waitpid(child_pid, &status, 0) == child_pid);

    printf("%s exit (pid = %d):", label, child_pid); /* and one line from */
    if (WIFEXITED(status)) {
        printf("	normal termination (exit code = %d)\n", WEXITSTATUS(status)); /* or */
    } else if (WIFSIGNALED(status)) {
        char *cd_text = "";
#ifdef WCOREDUMP
        if (WCOREDUMP(status)) {
            cd_text = "with core dump ";
        }
#endif
        printf("	signal termination %s(signal = %d)\n", cd_text, WTERMSIG(status)); /* or */

    } else {
        printf("	unknown type of termination\n");
    }


}

/* ARGSUSED */
int main(int argc, char *argv[]) {
    run_child(0, argc, argv);

    return 0;
}

