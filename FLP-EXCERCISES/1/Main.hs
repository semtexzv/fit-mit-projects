module Main where
    

main :: IO ()
main = putStrLn "a"

acker :: Integer -> Integer -> Integer
acker 0 y = y + 1
acker x 0 = acker (x-1) 1
acker x y = acker (x-1) (acker (x) (y-1))

fact :: Integer -> Integer
fact x 
    | x < 0  = error "Only positive"
    | x == 0 = 1
    | True   = fact(x-1) * x

fact2 :: Integer -> Integer
fact2 n = product [1 .. n]

fib :: Integer -> Integer
fib 0 = 0 
fib 1 = 1 
fib n = fib (n-1) + (fib (n-2))

fib' :: Integer -> Integer
fib' x  | x <  0 = error "Cant compute factorial"
        | x == 0 = 0
        | x == 1 = 1
        | otherwise = fib (x-1) + fib (x-2)

fib2 :: [Integer]
fib2 = [ a + b | a <- 1:fib2, b <- 0:1:fib2 ]


data List x = Head x (List x) | Tail deriving (Show)


fact' :: Integer -> Maybe Integer
fact' x | x < 0 = Nothing
        | x == 0 = Just 1
        | otherwise = Just (x * (fact (x-1)))